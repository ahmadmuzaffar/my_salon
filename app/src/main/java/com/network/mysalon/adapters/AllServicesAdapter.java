package com.network.mysalon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.network.mysalon.R;
import com.network.mysalon.models.Service;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.UserPreferences;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class AllServicesAdapter extends RecyclerView.Adapter<AllServicesAdapter.ViewHolder> {

    List<Service> services;
    Context context;
    SelectedServicesAdapter.ItemLongClick listener;
    ClickListener clickListener;
    UserModel user;

    public AllServicesAdapter(List<Service> services, Context context) {
        this.services = services;
        this.context = context;
        user = new UserPreferences(context).getUser();
    }

    public SelectedServicesAdapter.ItemLongClick getListener() {
        return listener;
    }

    public void setListener(SelectedServicesAdapter.ItemLongClick listener) {
        this.listener = listener;
    }

    public ClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_services_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Service service = services.get(position);

        holder.title.setText(service.getName());
        holder.price.setText(service.getPrice() + " Rs");
        holder.type.setText(service.getType());

        if (user.getRole().equals("user")) {
            holder.addToCart.setOnClickListener(view -> {
                if (clickListener != null)
                    clickListener.onAddToCartClick(service);
            });
        } else
            holder.addToCart.setVisibility(View.GONE);

        holder.rootContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                if (listener != null) {
                    listener.onLongPress(position);
                    return true;
                }

                return false;
            }
        });

        holder.rootContainer.setOnClickListener(v -> {
            if (clickListener != null) {
                clickListener.onClick(service);
            }
        });

    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView addToCart;
        TextView title, type, price;
        LinearLayout rootContainer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.serviceName);
            type = itemView.findViewById(R.id.serviceType);
            price = itemView.findViewById(R.id.servicePrice);
            addToCart = itemView.findViewById(R.id.signInCard);
            rootContainer = itemView.findViewById(R.id.rootContainer);

        }
    }

    public interface ClickListener {
        void onAddToCartClick(Service service);

        void onClick(Service service);
    }

}
