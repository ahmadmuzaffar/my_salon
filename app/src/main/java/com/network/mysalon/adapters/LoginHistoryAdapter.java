package com.network.mysalon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.network.mysalon.R;
import com.network.mysalon.models.LoginHistory;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.UserPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class LoginHistoryAdapter extends RecyclerView.Adapter<LoginHistoryAdapter.ViewHolder> {

    ArrayList<LoginHistory> loginHistories;
    Context context;
    UserModel userModel;

    public LoginHistoryAdapter(ArrayList<LoginHistory> loginHistories, Context context) {
        this.loginHistories = loginHistories;
        this.context = context;
        userModel = new UserPreferences(context).getUser();
    }

    public ArrayList<LoginHistory> getLoginHistories() {
        return loginHistories;
    }

    public void setLoginHistories(ArrayList<LoginHistory> loginHistories) {
        this.loginHistories = loginHistories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.session_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LoginHistory loginHistory = loginHistories.get(position);

        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(loginHistory.getLoginDateTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.dateLogin.setText(new SimpleDateFormat("dd MMM, yyyy").format(date));
        holder.timeLogin.setText(new SimpleDateFormat("hh:mm a").format(date));

        if (!loginHistory.getLogoutDateTime().isEmpty()) {
            Date date1 = null;
            try {
                date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(loginHistory.getLogoutDateTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            holder.dateLogout.setText(new SimpleDateFormat("dd MMM, yyyy").format(date1));
            holder.timeLogout.setText(new SimpleDateFormat("hh:mm a").format(date1));
        }

    }

    @Override
    public int getItemCount() {
        return loginHistories.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView dateLogin, timeLogin, dateLogout, timeLogout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            dateLogin = itemView.findViewById(R.id.dateLogin);
            timeLogin = itemView.findViewById(R.id.timeLogin);
            dateLogout = itemView.findViewById(R.id.dateLogout);
            timeLogout = itemView.findViewById(R.id.timeLogout);

        }
    }

}
