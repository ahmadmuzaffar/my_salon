package com.network.mysalon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.network.mysalon.R;
import com.network.mysalon.models.Bundle;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.UserPreferences;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AllBundlesAdapter extends RecyclerView.Adapter<AllBundlesAdapter.ViewHolder> {

    List<Bundle> bundles;
    Context context;
    SelectedServicesAdapter.ItemLongClick listener;
    AllBundlesAdapter.ClickListener clickListener;
    UserModel user;

    public AllBundlesAdapter(List<Bundle> bundles, Context context) {
        this.bundles = bundles;
        this.context = context;
        user = new UserPreferences(context).getUser();
    }

    public SelectedServicesAdapter.ItemLongClick getListener() {
        return listener;
    }

    public void setListener(SelectedServicesAdapter.ItemLongClick listener) {
        this.listener = listener;
    }

    public ClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_bundles_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Bundle bundle = bundles.get(position);

        holder.title.setText(bundle.getName());
        holder.price.setText(bundle.getPrice() + " Rs");

        SelectedServicesAdapter adapter = new SelectedServicesAdapter(bundle.getServiceIds(), context);
        adapter.setItemLongClick(null);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        holder.selectedServices.setItemAnimator(new DefaultItemAnimator());
        holder.selectedServices.setLayoutManager(layoutManager);
        holder.selectedServices.setAdapter(adapter);

        holder.rootContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (listener != null) {
                    listener.onLongPress(position);
                    return true;
                }

                return false;
            }
        });

        if (user.getRole().equals("user")) {
            holder.addToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        clickListener.onAddToCartClick(bundle);
                    }
                }
            });
        } else
            holder.addToCart.setVisibility(View.GONE);

        holder.rootContainer.setOnClickListener(v -> {
            if (clickListener != null) {
                clickListener.onClick(bundle);
            }
        });

    }

    @Override
    public int getItemCount() {
        return bundles.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView addToCart;
        TextView title, price;
        RecyclerView selectedServices;
        LinearLayout rootContainer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.bundleName);
            price = itemView.findViewById(R.id.bundlePrice);
            addToCart = itemView.findViewById(R.id.signInCard);
            selectedServices = itemView.findViewById(R.id.selectedServices);
            rootContainer = itemView.findViewById(R.id.rootContainer);

        }
    }

    public interface ClickListener {
        void onAddToCartClick(Bundle bundle);

        void onClick(Bundle bundle);
    }

}
