package com.network.mysalon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.network.mysalon.R;
import com.network.mysalon.utilities.SquareRelativeLayout;
import com.network.mysalon.models.UserModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SalonAdapter extends RecyclerView.Adapter<SalonAdapter.ViewHolder> {

    ArrayList<UserModel> salonItems;
    Context context;
    ItemClickListener itemClickListener;

    public SalonAdapter(ArrayList<UserModel> salonItems, Context context) {
        this.salonItems = salonItems;
        this.context = context;
    }

    public ItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public ArrayList<UserModel> getSalonItems() {
        return salonItems;
    }

    public void setSalonItems(ArrayList<UserModel> salonItems) {
        this.salonItems = salonItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.salon_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserModel salonItem = salonItems.get(position);

        holder.salonText.setText(salonItem.getNameOnBoard());
        if(salonItem.getProfileImage() != null) {
            Glide.with(context).load(salonItem.getProfileImage())
                    .into(holder.salonLogo);
        }

        holder.rootContainer.setOnClickListener(v -> {
            itemClickListener.onClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return salonItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView salonLogo;
        TextView salonText;
        SquareRelativeLayout rootContainer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            salonLogo = itemView.findViewById(R.id.salonLogo);
            salonText = itemView.findViewById(R.id.salonName);
            rootContainer = itemView.findViewById(R.id.rootContainer);

        }
    }

    public interface ItemClickListener {
        void onClick(int position);
    }

}
