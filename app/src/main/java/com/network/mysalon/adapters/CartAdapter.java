package com.network.mysalon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.network.mysalon.R;
import com.network.mysalon.models.CartItem;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    List<CartItem> items;
    Context context;
    RemoveItem removeItem;
    boolean hideRemove = false;

    public CartAdapter(List<CartItem> items, Context context) {
        this.items = items;
        this.context = context;
    }

    public boolean isHideRemove() {
        return hideRemove;
    }

    public void setHideRemove(boolean hideRemove) {
        this.hideRemove = hideRemove;
    }

    public RemoveItem getRemoveItem() {
        return removeItem;
    }

    public void setRemoveItem(RemoveItem removeItem) {
        this.removeItem = removeItem;
    }

    public List<CartItem> getItems() {
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CartItem cartItem = items.get(position);

        holder.title.setText(cartItem.getTitle());
        holder.price.setText(cartItem.getPrice() + "");
        holder.type.setText(cartItem.getType());

        if (hideRemove) {
            holder.remove.setVisibility(View.INVISIBLE);
        } else {
            holder.remove.setOnClickListener(v -> {
                removeItem.remove(position);
            });
        }

    }

    public CartItem getItem(int pos) {
        return items.get(pos);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, type, price;
        ImageView remove;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            price = itemView.findViewById(R.id.price);
            type = itemView.findViewById(R.id.type);
            remove = itemView.findViewById(R.id.removeItem);

        }
    }

    public interface RemoveItem {
        void remove(int position);
    }

}
