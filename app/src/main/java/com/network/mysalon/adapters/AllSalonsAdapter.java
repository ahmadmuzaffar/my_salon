package com.network.mysalon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.network.mysalon.R;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.models.UserModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class AllSalonsAdapter extends RecyclerView.Adapter<AllSalonsAdapter.ViewHolder> {

    ArrayList<UserModel> salonItems;
    Context context;
    SalonActions salonActions;
    UserModel userModel;

    public void setSalonActions(SalonActions salonActions) {
        this.salonActions = salonActions;
    }

    public AllSalonsAdapter(ArrayList<UserModel> salonItems, Context context) {
        this.salonItems = salonItems;
        this.context = context;
        userModel = new UserPreferences(context).getUser();
    }

    public ArrayList<UserModel> getSalonItems() {
        return salonItems;
    }

    public void setSalonItems(ArrayList<UserModel> salonItems) {
        this.salonItems = salonItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_salon_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserModel salonItem = salonItems.get(position);

        holder.title.setText(salonItem.getNameOnBoard());
        holder.phone.setText(salonItem.getPhoneNumber());
        Glide.with(context)
                .load(salonItem.getProfileImage())
                .placeholder(R.drawable.logo)
                .into(holder.salonPicture);

        if (salonItem.getLikeGivers().contains(userModel.getId())) {
            holder.btnLike.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_filled));
        }

        if (salonItem.getReviewGivers().contains(userModel.getId())) {
            holder.btnRate.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_rate));
        }

        if (salonItem.getReviewGivers().size() > 0) {
            float rating = Float.parseFloat(salonItem.getRating()) / (float) salonItem.getReviewGivers().size();
            holder.ratingBar.setRating(rating);
            holder.reviews.setText("(" + salonItem.getReviewGivers().size() + " Review(s))");
        } else {
            holder.ratingBar.setRating(0f);
            holder.reviews.setText("(0 Review)");
        }
        if (salonItem.getLikeGivers().size() > 0)
            holder.likes.setText(salonItem.getLikeGivers().size() + " Like(s)");
        else
            holder.likes.setText(0 + " Likes)");

        holder.rootContainer.setOnClickListener(view -> {
            salonActions.openSalonDetail(salonItem);
        });

        holder.btnLike.setOnClickListener(view -> {
            salonActions.performLike(salonItem);
        });

        holder.btnRate.setOnClickListener(v -> {
            salonActions.performRate(salonItem);
        });

    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return salonItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView salonPicture, btnLike, btnRate;
        TextView title, phone, reviews, likes;
        RelativeLayout rootContainer;
        RatingBar ratingBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            salonPicture = itemView.findViewById(R.id.profilePicture);
            title = itemView.findViewById(R.id.title);
            phone = itemView.findViewById(R.id.phone);
            reviews = itemView.findViewById(R.id.reviews);
            likes = itemView.findViewById(R.id.likes);
            btnLike = itemView.findViewById(R.id.btnLike);
            btnRate = itemView.findViewById(R.id.btnRate);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            rootContainer = itemView.findViewById(R.id.rootContainer);

        }
    }

    public interface SalonActions {
        void performRate(UserModel obj);

        void performLike(UserModel obj);

        void openSalonDetail(UserModel obj);
    }

}
