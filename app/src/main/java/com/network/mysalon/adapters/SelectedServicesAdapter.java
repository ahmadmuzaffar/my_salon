package com.network.mysalon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.network.mysalon.R;
import com.network.mysalon.models.SelectedService;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SelectedServicesAdapter extends RecyclerView.Adapter<SelectedServicesAdapter.ViewHolder> {

    List<SelectedService> selectedServices;
    Context context;
    ItemLongClick itemLongClick;

    public SelectedServicesAdapter(List<SelectedService> selectedServices, Context context) {
        this.selectedServices = selectedServices;
        this.context = context;
    }

    public ItemLongClick getItemLongClick() {
        return itemLongClick;
    }

    public void setItemLongClick(ItemLongClick itemLongClick) {
        this.itemLongClick = itemLongClick;
    }

    public List<SelectedService> getSelectedServices() {
        return selectedServices;
    }

    public void setSelectedServices(List<SelectedService> selectedServices) {
        this.selectedServices = selectedServices;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.selected_services_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SelectedService selectedService = selectedServices.get(position);

        holder.title.setText(selectedService.getTitle());
        holder.price.setText(selectedService.getPrice() + "");

        holder.rootContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (itemLongClick != null) {
                    itemLongClick.onLongPress(position);
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return selectedServices.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, price;
        LinearLayout rootContainer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            price = itemView.findViewById(R.id.price);
            rootContainer = itemView.findViewById(R.id.rootContainer);

        }
    }

    public interface ItemLongClick {
        void onLongPress(int position);
    }

}
