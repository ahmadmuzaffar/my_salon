package com.network.mysalon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.network.mysalon.R;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.models.Notifications;
import com.network.mysalon.models.UserModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    ArrayList<Notifications> notifications;
    Context context;
    OpenNotification openNotification;
    UserModel userModel;

    public void setOpenNotification(OpenNotification openNotification) {
        this.openNotification = openNotification;
    }

    public NotificationAdapter(ArrayList<Notifications> notifications, Context context) {
        this.notifications = notifications;
        this.context = context;
        userModel = new UserPreferences(context).getUser();
    }

    public ArrayList<Notifications> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<Notifications> notifications) {
        this.notifications = notifications;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Notifications notification = notifications.get(position);

        Glide.with(context)
                .load(notification.getNotificationByImage())
                .placeholder(R.drawable.logo)
                .into(holder.profilePic);

        if (notification.isNotificationRead())
            holder.rootContainer.setBackgroundResource(R.color.colorBg);
        holder.title.setText(notification.getNotificationTitle());
        holder.body.setText(notification.getNotificationBody());
        try {
            holder.date.setText(new SimpleDateFormat("HH:mm a - dd/MM").
                    format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").
                            parse(notification.getNotificationDateTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.rootContainer.setOnClickListener(v -> {
            openNotification.readNotification(notification);
        });

    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profilePic;
        TextView title, body, date;
        RelativeLayout rootContainer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            profilePic = itemView.findViewById(R.id.profilePic);
            title = itemView.findViewById(R.id.title);
            body = itemView.findViewById(R.id.body);
            date = itemView.findViewById(R.id.datetime);
            rootContainer = itemView.findViewById(R.id.rootContainer);

        }
    }

    public interface OpenNotification {
        void readNotification(Notifications notification);
    }

}
