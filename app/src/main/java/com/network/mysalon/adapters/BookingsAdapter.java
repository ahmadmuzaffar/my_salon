package com.network.mysalon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.network.mysalon.R;
import com.network.mysalon.models.Booking;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BookingsAdapter extends RecyclerView.Adapter<BookingsAdapter.ViewHolder> {

    List<Booking> bookings;
    Context context;
    ClickListener clickListener;

    public BookingsAdapter(List<Booking> bookings, Context context, ClickListener clickListener) {
        this.bookings = bookings;
        this.context = context;
        this.clickListener = clickListener;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Booking booking = bookings.get(position);

        holder.id.setText(booking.getId());
        holder.customer.setText(booking.getUser().getFullName());
        holder.status.setText(booking.getStatus().toString());
        holder.date.setText(booking.getScheduledDateTime());
        try {
            holder.bookingDateTime.setText(new SimpleDateFormat("HH:mm a - dd/MM").
                    format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").
                            parse(booking.getDateTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.price.setText(booking.getTotalPayment() + "");

        holder.details.setOnClickListener(v -> {
            clickListener.onClick(position);
        });

    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView details;
        private TextView id, customer, status, date, price, bookingDateTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            details = itemView.findViewById(R.id.details);
            id = itemView.findViewById(R.id.id);
            customer = itemView.findViewById(R.id.customer);
            status = itemView.findViewById(R.id.status);
            date = itemView.findViewById(R.id.date);
            price = itemView.findViewById(R.id.price);
            bookingDateTime = itemView.findViewById(R.id.bookingDateTime);
        }
    }

    public interface ClickListener {
        void onClick(int pos);
    }

}
