package com.network.mysalon.fragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.network.mysalon.R;
import com.network.mysalon.adapters.CartAdapter;
import com.network.mysalon.databinding.FragmentCartBinding;
import com.network.mysalon.models.Activities;
import com.network.mysalon.models.Booking;
import com.network.mysalon.models.CartItem;
import com.network.mysalon.models.Service;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.FcmNotifier;
import com.network.mysalon.utilities.UserPreferences;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CartFragment extends Fragment implements Serializable, CartAdapter.RemoveItem {

    FragmentCartBinding mBinding;
    Booking booking;
    CartAdapter adapter;
    List<CartItem> itemList = new ArrayList<>();
    UserModel user;
    double amount = 0.0;

    public CartFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false);
        booking = new UserPreferences(getContext()).getCartData();
        user = booking.getUser();

        populateData();

        mBinding.dateTime.setOnClickListener(v -> {
            new SingleDateAndTimePickerDialog.Builder(getContext())
                    .bottomSheet()
                    .curved()
                    .displayHours(true)
                    .displayMinutes(true)
                    .displayAmPm(true)
                    .mustBeOnFuture()
                    .title("Select Date and Time")
                    .listener(date -> {
                        Date date1 = null;
                        try {
                            date1 = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy").parse(date.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        mBinding.dateTime.setText(new SimpleDateFormat("dd MMM, yyyy - hh:mm a").format(date1));
                    }).display();

        });

        mBinding.clearCart.setOnClickListener(v -> {
            if (new UserPreferences(getContext()).setCartData(null))
                getActivity().onBackPressed();
        });

        mBinding.checkout.setOnClickListener(v -> {

            if (mBinding.dateTime.length() == 0) {
                Toast.makeText(getContext(), "Please select date time", Toast.LENGTH_SHORT).show();
                return;
            }

            Booking booking = new UserPreferences(getContext()).getCartData();
            final DatabaseReference database = FirebaseDatabase.getInstance().getReference().child("bookings");
            booking.setId(database.push().getKey());
            booking.setTotalPayment(Double.parseDouble(mBinding.totalPrice.getText().toString()));
            booking.setStatus(Booking.STATUS.REQUEST);
            booking.setScheduledDateTime(mBinding.dateTime.getText().toString());
            booking.setDateTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date()));
            database.child(booking.getId()).setValue(booking);

            user.getActivities().add(new Activities("Booking", "You made a new booking to " + booking.getSalon().getNameOnBoard() + "'s services", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
            FcmNotifier.sendNotification("You have a new booking from " + user.getFullName(), "Booking", user, booking.getSalon());
            FirebaseDatabase.getInstance().getReference().child("users/" + user.getRole() + "/" + user.getId()).setValue(user);

            new UserPreferences(getContext()).setUser(user);
            if (new UserPreferences(getContext()).setCartData(null))
                getActivity().onBackPressed();
        });

        return mBinding.getRoot();
    }

    private void populateData() {

        mBinding.cName.setText(booking.getUser().getFullName());
        mBinding.sName.setText(booking.getSalon().getNameOnBoard());
        setCartAdapter();

    }

    private void setCartAdapter() {

        for (Service service : booking.getServices()) {
            CartItem cartItem = new CartItem(service.getName(), "service", service.getPrice());
            amount += service.getPrice();
            itemList.add(cartItem);
        }
        for (com.network.mysalon.models.Bundle bundle : booking.getBundles()) {
            CartItem cartItem = new CartItem(bundle.getName(), "bundle", bundle.getPrice());
            amount += bundle.getPrice();
            itemList.add(cartItem);
        }
        mBinding.totalPrice.setText(String.valueOf(amount));

        adapter = new CartAdapter(itemList, getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        adapter.setRemoveItem(this);
        mBinding.cartItems.setItemAnimator(new DefaultItemAnimator());
        mBinding.cartItems.setLayoutManager(layoutManager);
        mBinding.cartItems.setAdapter(adapter);

    }

    @Override
    public void remove(int position) {
        CartItem item = adapter.getItem(position);
        List<Service> services = booking.getServices();
        List<com.network.mysalon.models.Bundle> bundles = booking.getBundles();

        if (item.getType().toLowerCase().equals("service")) {
            for (Service service : services) {
                if (item.getTitle().equals(service.getName())) {
                    booking.getServices().remove(service);
                    amount -= service.getPrice();
                    break;
                }
            }
        } else {
            for (com.network.mysalon.models.Bundle bundle : bundles) {
                if (item.getTitle().equals(bundle.getName())) {
                    booking.getBundles().remove(bundle);
                    amount -= bundle.getPrice();
                    break;
                }
            }
        }
        adapter.getItems().remove(item);
        adapter.notifyItemRemoved(position);

        mBinding.totalPrice.setText(String.valueOf(amount));
    }
}