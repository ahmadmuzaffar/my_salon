package com.network.mysalon.fragments.salon;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import cn.bingerz.android.fastlocation.FastLocation;
import cn.bingerz.android.fastlocation.LocationResultListener;

import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.network.mysalon.R;
import com.network.mysalon.models.Activities;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.activities.PreviewActivity;
import com.network.mysalon.databinding.FragmentVendorProfileBinding;
import com.network.mysalon.models.UserModel;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.LOCATION_SERVICE;

public class SalonProfileFragment extends Fragment implements LocationListener, Serializable {

    private static final int PICK_IMAGE_CODE = 1001;
    private static final int CAMERA_PIC_REQUEST_CODE = 1003;
    private Uri photoURI;
    FragmentVendorProfileBinding mBinding;
    UserModel userModel;
    private String imagePath;
    private LocationManager mLocationManager;
    private File file;
    private Dialog d;
    private FastLocation fastLocation;

    public SalonProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_vendor_profile, container, false);

        userModel = new UserPreferences(getContext()).getUser();
        populateData();

        mBinding.btnCapture.setOnClickListener(v -> {
            showPictureDialog();
        });

        mBinding.profilePicture.setOnClickListener(v -> {
            if (photoURI != null)
                startActivity(new Intent(getActivity(), PreviewActivity.class).putExtra("imageUri", photoURI.toString()));
            else if (userModel.getProfileImage() != null)
                startActivity(new Intent(getActivity(), PreviewActivity.class).putExtra("imageUrl", userModel.getProfileImage()));
        });

        mBinding.btnEdit.setOnClickListener(v -> {
            mBinding.btnCapture.setVisibility(View.VISIBLE);
            mBinding.etName.setEnabled(true);
            mBinding.etPhone.setEnabled(true);
            mBinding.nameOnBoard.setEnabled(true);
            mBinding.etXCoordinate.setEnabled(true);
            mBinding.etYCoordinate.setEnabled(true);
            mBinding.btnUpdate.setEnabled(true);
            mBinding.locationLabel.setEnabled(true);
        });

        mBinding.btnUpdate.setOnClickListener(v -> {
            mBinding.btnCapture.setVisibility(View.GONE);
            mBinding.etName.setEnabled(false);
            mBinding.etPhone.setEnabled(false);
            mBinding.nameOnBoard.setEnabled(false);
            mBinding.etXCoordinate.setEnabled(false);
            mBinding.etYCoordinate.setEnabled(false);
            mBinding.btnUpdate.setEnabled(false);

            if (TextUtils.isEmpty(mBinding.etName.getText()) ||
                    TextUtils.isEmpty(mBinding.etPhone.getText()) ||
                    TextUtils.isEmpty(mBinding.nameOnBoard.getText()) ||
                    TextUtils.isEmpty(mBinding.etXCoordinate.getText()) ||
                    TextUtils.isEmpty(mBinding.etYCoordinate.getText())) {
                Toast.makeText(getContext(), "Please enter all fields to update", Toast.LENGTH_SHORT).show();
                return;
            }

            d = new Dialog(getContext());
            d.setContentView(R.layout.dialog_loader);
            d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            d.setCancelable(false);
            d.setCanceledOnTouchOutside(false);

            d.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    updateProfile();
                }
            });

            d.show();

        });

        mBinding.locationLabel.setOnClickListener(v -> {
            mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            fastLocation = new FastLocation(getContext());

            boolean gps_enabled = false;

            try {
                gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (!gps_enabled) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Please turn on location")
                        .setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
            } else {
                d = new Dialog(getContext());
                d.setContentView(R.layout.dialog_loader);
                d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                d.setCancelable(false);
                d.setCanceledOnTouchOutside(false);

                d.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(getActivity(), "Permission not granted", Toast.LENGTH_SHORT).show();
                            d.dismiss();
                        } else {
                            fastLocation.getLocation(new LocationResultListener() {
                                @Override
                                public void onResult(Location location) {
                                    mBinding.etXCoordinate.setText(String.valueOf(location.getLatitude()));
                                    mBinding.etYCoordinate.setText(String.valueOf(location.getLongitude()));
                                    d.dismiss();
                                }
                            });
                        }
//                        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, SalonProfileFragment.this);
                    }
                });

                d.show();
            }
        });

        return mBinding.getRoot();
    }

    private void updateProfile() {

        userModel.setFullName(mBinding.etName.getText().toString());
        userModel.setPhoneNumber(mBinding.etPhone.getText().toString());
        userModel.setNameOnBoard(mBinding.nameOnBoard.getText().toString());
        userModel.setLatitude(mBinding.etXCoordinate.getText().toString());
        userModel.setLongitude(mBinding.etYCoordinate.getText().toString());

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("users/vendor/" + userModel.getId());

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userModel = dataSnapshot.getValue(UserModel.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        if (photoURI != null) {
            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            StorageReference imageRef = storageRef.child("Images/" + userModel.getId() + "/ProfileImages/" + photoURI.getLastPathSegment());
            UploadTask uploadTask = imageRef.putFile(photoURI);

            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
//                    database.getReference().child("users/vendor/" + userModel.getId()).setValue(userModel);
                    d.dismiss();
                    Log.e("Exception", "updateProfile: ", task.getException());
                    return null;
                }

                return imageRef.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    userModel.setProfileImage(downloadUri.toString());
                }
                userModel.getActivities().add(new Activities("Profile Updated", "You updated your profile", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                new UserPreferences(getContext()).setUser(userModel);
                database.getReference().child("users/vendor/" + userModel.getId()).setValue(userModel);
                d.dismiss();
            });
        } else {
            userModel.getActivities().add(new Activities("Profile Updated", "You updated your profile", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
            new UserPreferences(getContext()).setUser(userModel);
            database.getReference().child("users/vendor/" + userModel.getId()).setValue(userModel);
            d.dismiss();
        }

    }

    private void populateData() {

        mBinding.etName.setText(userModel.getFullName());
        mBinding.etEmail.setText(userModel.getEmailAddress());
        mBinding.etPhone.setText(userModel.getPhoneNumber());
        mBinding.nameOnBoard.setText(userModel.getNameOnBoard());
        mBinding.etXCoordinate.setText(userModel.getLatitude());
        mBinding.etYCoordinate.setText(userModel.getLongitude());
        mBinding.etPassword.setText(userModel.getPassword());

        if (userModel.getProfileImage() != null) {
            Glide.with(getContext())
                    .load(userModel.getProfileImage())
                    .into(mBinding.profilePicture);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_IMAGE_CODE) {
                photoURI = data.getData();

                imagePath = photoURI.getPath();
                Cursor cursor = null;
                try {
                    String[] proj = {MediaStore.Images.Media.DATA};
                    cursor = getActivity().getContentResolver().query(photoURI, proj, null, null, null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    imagePath = cursor.getString(column_index);
                } catch (Exception e) {
                    imagePath = photoURI.getPath();
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
                mBinding.profilePicture.setImageURI(photoURI);
            } else if (requestCode == CAMERA_PIC_REQUEST_CODE) {
                photoURI = FileProvider.getUriForFile(getContext(), getActivity().getApplicationContext().getPackageName() + ".fileprovider", file);
                mBinding.profilePicture.setImageURI(photoURI);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            }
        } else if (requestCode == 2) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera();
            }
        }
    }

    @SuppressLint("IntentReset")
    public void showPictureDialog() {

        final Dialog d = new Dialog(getContext());
        d.setContentView(R.layout.dialog_capture);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(d.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        d.show();
        d.getWindow().setAttributes(layoutParams);

        TextView btnCamera, btnGallery;
        btnCamera = d.findViewById(R.id.btnCamera);
        btnGallery = d.findViewById(R.id.btnGallery);

        btnCamera.setOnClickListener(v -> {
            if (checkCameraPermission()) {
                openCamera();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        2);
            }
            d.dismiss();
        });

        btnGallery.setOnClickListener(v -> {
            if (checkReadPermission() && checkWritePermission()) {
                openGallery();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
            d.dismiss();
        });
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, PICK_IMAGE_CODE);
    }

    private void openCamera() {
        Intent m_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        Uri uri = FileProvider.getUriForFile(getContext(), getActivity().getApplicationContext().getPackageName() + ".fileprovider", file);
        m_intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(m_intent, CAMERA_PIC_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        String permission = Manifest.permission.CAMERA;
        int res = getActivity().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkReadPermission() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        int res = getActivity().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkWritePermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getActivity().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        mBinding.etXCoordinate.setText(location.getLatitude() + "");
        mBinding.etYCoordinate.setText(location.getLongitude() + "");
        mLocationManager.removeUpdates(SalonProfileFragment.this);
        d.dismiss();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }
}