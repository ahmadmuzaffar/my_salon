package com.network.mysalon.fragments.salon;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.network.mysalon.R;
import com.network.mysalon.activities.PreviewActivity;
import com.network.mysalon.adapters.SelectedServicesAdapter;
import com.network.mysalon.databinding.FragmentAddServiceBinding;
import com.network.mysalon.dialogs.SweetAlertDialogs;
import com.network.mysalon.models.Activities;
import com.network.mysalon.models.SelectedService;
import com.network.mysalon.models.Service;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.UserPreferences;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddServiceFragment extends Fragment implements Serializable, SelectedServicesAdapter.ItemLongClick {

    private static final int PICK_IMAGE_CODE = 1001;
    private static final int CAMERA_PIC_REQUEST_CODE = 1003;
    File file = null;
    SelectedServicesAdapter adapter;
    List<SelectedService> selectedServices = new ArrayList<>();
    FragmentAddServiceBinding mBinding;
    UserModel user;
    private Uri photoURI;
    private String imagePath;
    String serviceType;
    private Dialog d;
    private String action;
    private Service service;
    private com.network.mysalon.models.Bundle servicePackage;

    public AddServiceFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_service, container, false);

        user = new UserPreferences(getContext()).getUser();

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        mBinding.image.setOnClickListener(v -> {
            if (photoURI != null)
                startActivity(new Intent(getActivity(), PreviewActivity.class).putExtra("imageUri", photoURI.toString()));
            else {
                if (serviceType.equals("service") && service.getImage() != null) {
                    startActivity(new Intent(getActivity(), PreviewActivity.class).putExtra("imageUrl", service.getImage()));
                } else if (servicePackage.getImage() != null)
                    startActivity(new Intent(getActivity(), PreviewActivity.class).putExtra("imageUrl", servicePackage.getImage()));
            }
        });

        Bundle bundle = getArguments();
        action = bundle.containsKey("action") ? bundle.getString("action") : null;
        serviceType = bundle.containsKey("service_type") ? bundle.getString("service_type") : null;
        if (action != null) {
            if (serviceType.equals("service")) {

                mBinding.header.setText(getString(R.string.service));
                mBinding.btnAdd.setText(getString(R.string.service));

                service = (Service) bundle.getSerializable("service");
                if (service.getImage() != null) {
                    Glide.with(getContext()).load(service.getImage()).into(mBinding.image);
                }
                mBinding.serviceName.setText(service.getName());
                mBinding.type.setText(service.getType(), false);
                mBinding.serviceDesc.setText(service.getDescription());
                mBinding.duration.setText(service.getDuration());
                mBinding.price.setText(String.valueOf(service.getPrice()));

                setSpinner();

                mBinding.btnAdd.setText("Update Service");
            } else {
                mBinding.typeCard.setVisibility(View.GONE);
                mBinding.durationCard.setVisibility(View.GONE);
                mBinding.packageLayout.setVisibility(View.VISIBLE);
                mBinding.header.setText(getString(R.string.packages));
                mBinding.btnAdd.setText(getString(R.string.packages));

                servicePackage = (com.network.mysalon.models.Bundle) bundle.getSerializable("package");
                if (servicePackage.getImage() != null) {
                    Glide.with(getContext()).load(servicePackage.getImage()).into(mBinding.image);
                }
                mBinding.serviceName.setText(servicePackage.getName());
                mBinding.serviceDesc.setText(servicePackage.getDescription());
                mBinding.price.setText(String.valueOf(servicePackage.getPrice()));

                setPackageSpinner();
                selectedServices = servicePackage.getServiceIds();
                setAdapter();

                mBinding.btnAdd.setText("Update Package");
            }
        } else {
            if (serviceType.equals("package")) {
                mBinding.typeCard.setVisibility(View.GONE);
                mBinding.durationCard.setVisibility(View.GONE);
                mBinding.packageLayout.setVisibility(View.VISIBLE);
                mBinding.header.setText(getString(R.string.add_package));
                mBinding.btnAdd.setText(getString(R.string.add_package));

                setPackageSpinner();
                setAdapter();
            } else {
                setSpinner();
            }
        }

        mBinding.serviceSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String text = (String) adapterView.getItemAtPosition(i);
                SelectedService selectedService = new SelectedService();
                selectedService.setTitle(text);
                for (Service service : user.getServices()) {
                    if (service.getName().equals(text))
                        selectedService.setPrice(service.getPrice());
                }
                selectedServices.add(selectedService);
                adapter.setSelectedServices(selectedServices);
                adapter.notifyDataSetChanged();
            }
        });

        mBinding.serviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SelectedService selectedService = new SelectedService();
                selectedService.setTitle(mBinding.serviceSpinner.getText().toString());
                for (Service service : user.getServices()) {
                    if (service.getName().equals(mBinding.serviceSpinner.getText().toString())) {
                        selectedService.setId(service.getId());
                        selectedService.setPrice(service.getPrice());
                    }
                }
                selectedServices.add(selectedService);
                adapter.setSelectedServices(selectedServices);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBinding.btnCapture.setOnClickListener(v -> {
            showPictureDialog();
        });

        mBinding.btnAdd.setOnClickListener(v -> {
            if (action != null) {
                if (!serviceType.equals("package")) {
                    if (mBinding.serviceName.length() == 0 || mBinding.serviceDesc.length() == 0 || mBinding.price.length() == 0 || mBinding.duration.length() == 0 || mBinding.type.length() == 0) {
                        Toast.makeText(getContext(), "All fields are required", Toast.LENGTH_SHORT).show();
                    } else {
                        d = new Dialog(getContext());
                        d.setContentView(R.layout.dialog_loader);
                        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        d.setCancelable(false);
                        d.setCanceledOnTouchOutside(false);

                        d.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialogInterface) {
                                service.setName(mBinding.serviceName.getText().toString());
                                service.setDescription(mBinding.serviceDesc.getText().toString());
                                service.setDuration(mBinding.duration.getText().toString() + " Mins");
                                service.setPrice(Double.parseDouble(mBinding.price.getText().toString()));
                                service.setType(mBinding.type.getText().toString());

                                if (photoURI != null) {
                                    StorageReference storageRef = FirebaseStorage.getInstance().getReference();
                                    StorageReference imageRef = storageRef.child("Images/" + user.getId() + "/services/" + photoURI.getLastPathSegment());
                                    UploadTask uploadTask = imageRef.putFile(photoURI);

                                    uploadTask.continueWithTask(task -> {
                                        if (!task.isSuccessful()) {
                                            d.dismiss();
                                            return null;
                                        }

                                        return imageRef.getDownloadUrl();
                                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Uri> task) {
                                            if (task.isSuccessful()) {
                                                Uri downloadUri = task.getResult();
                                                service.setImage(downloadUri.toString());
                                            }
                                            user.getActivities().add(new Activities("Service Update", "You updated a Service - " + service.getId(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                                            Service service2 = null;
                                            for (Service service1 : user.getServices()) {
                                                if (service.getId().equals(service1.getId())) {
                                                    service2 = service1;
                                                    break;
                                                }
                                            }
                                            user.getServices().remove(service2);
                                            user.getServices().add(service);
                                            new UserPreferences(getContext()).setUser(user);
                                            FirebaseDatabase.getInstance().getReference("users/vendor/" + user.getId()).setValue(user);
                                            d.dismiss();
                                            getActivity().onBackPressed();
                                        }
                                    });
                                } else {
                                    user.getActivities().add(new Activities("Service Update", "You updated a Service - " + service.getId(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                                    Service service2 = null;
                                    for (Service service1 : user.getServices()) {
                                        if (service.getId().equals(service1.getId())) {
                                            service2 = service1;
                                            break;
                                        }
                                    }
                                    user.getServices().remove(service2);
                                    user.getServices().add(service);
                                    new UserPreferences(getContext()).setUser(user);
                                    FirebaseDatabase.getInstance().getReference("users/vendor/" + user.getId()).setValue(user);
                                    d.dismiss();
                                    getActivity().onBackPressed();
                                }
                            }
                        });

                        d.show();
                    }
                } else {
                    if (mBinding.serviceName.length() == 0 || mBinding.serviceDesc.length() == 0 || mBinding.price.length() == 0) {
                        Toast.makeText(getContext(), "All fields are required", Toast.LENGTH_SHORT).show();
                    } else if (selectedServices.size() < 2) {
                        Toast.makeText(getContext(), "Atleast select 2 services to add package", Toast.LENGTH_SHORT).show();
                    } else {
                        d = new Dialog(getContext());
                        d.setContentView(R.layout.dialog_loader);
                        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        d.setCancelable(false);
                        d.setCanceledOnTouchOutside(false);

                        d.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialogInterface) {
                                servicePackage.setName(mBinding.serviceName.getText().toString());
                                servicePackage.setDescription(mBinding.serviceDesc.getText().toString());
                                servicePackage.setPrice(Double.parseDouble(mBinding.price.getText().toString()));
                                servicePackage.setServiceIds(selectedServices);

                                if (photoURI != null) {
                                    StorageReference storageRef = FirebaseStorage.getInstance().getReference();
                                    StorageReference imageRef = storageRef.child("Images/" + user.getId() + "/services/" + photoURI.getLastPathSegment());
                                    UploadTask uploadTask = imageRef.putFile(photoURI);

                                    uploadTask.continueWithTask(task -> {
                                        if (!task.isSuccessful()) {
                                            d.dismiss();
                                            return null;
                                        }

                                        return imageRef.getDownloadUrl();
                                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Uri> task) {
                                            if (task.isSuccessful()) {
                                                Uri downloadUri = task.getResult();
                                                servicePackage.setImage(downloadUri.toString());
                                            }
                                            user.getActivities().add(new Activities("Package Update", "You updated a Service - " + servicePackage.getId(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                                            com.network.mysalon.models.Bundle service2 = null;
                                            for (com.network.mysalon.models.Bundle service1 : user.getBundles()) {
                                                if (servicePackage.getId().equals(service1.getId())) {
                                                    service2 = service1;
                                                    break;
                                                }
                                            }
                                            user.getBundles().remove(service2);
                                            user.getBundles().add(servicePackage);
                                            new UserPreferences(getContext()).setUser(user);
                                            FirebaseDatabase.getInstance().getReference("users/vendor/" + user.getId()).setValue(user);
                                            d.dismiss();
                                            getActivity().onBackPressed();
                                        }
                                    });
                                } else {
                                    user.getActivities().add(new Activities("Package Update", "You updated a Service - " + servicePackage.getId(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                                    com.network.mysalon.models.Bundle service2 = null;
                                    for (com.network.mysalon.models.Bundle service1 : user.getBundles()) {
                                        if (servicePackage.getId().equals(service1.getId())) {
                                            service2 = service1;
                                            break;
                                        }
                                    }
                                    user.getBundles().remove(service2);
                                    user.getBundles().add(servicePackage);
                                    new UserPreferences(getContext()).setUser(user);
                                    FirebaseDatabase.getInstance().getReference("users/vendor/" + user.getId()).setValue(user);
                                    d.dismiss();
                                    getActivity().onBackPressed();
                                }
                            }
                        });

                        d.show();
                    }
                }
            } else {

                if (!serviceType.equals("package")) {
                    if (mBinding.serviceName.length() == 0 || mBinding.serviceDesc.length() == 0 || mBinding.price.length() == 0 || mBinding.duration.length() == 0 || mBinding.type.length() == 0) {
                        Toast.makeText(getContext(), "All fields are required", Toast.LENGTH_SHORT).show();
                    } else {
                        d = new Dialog(getContext());
                        d.setContentView(R.layout.dialog_loader);
                        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        d.setCancelable(false);
                        d.setCanceledOnTouchOutside(false);

                        d.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialogInterface) {
                                addService();
                            }
                        });

                        d.show();
                    }
                } else {
                    if (mBinding.serviceName.length() == 0 || mBinding.serviceDesc.length() == 0 || mBinding.price.length() == 0) {
                        Toast.makeText(getContext(), "All fields are required", Toast.LENGTH_SHORT).show();
                    } else if (selectedServices.size() < 2) {
                        Toast.makeText(getContext(), "Atleast select 2 services to add package", Toast.LENGTH_SHORT).show();
                    } else {
                        d = new Dialog(getContext());
                        d.setContentView(R.layout.dialog_loader);
                        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        d.setCancelable(false);
                        d.setCanceledOnTouchOutside(false);

                        d.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialogInterface) {
                                addPackage();
                            }
                        });

                        d.show();
                    }
                }
            }
        });

        return mBinding.getRoot();
    }

    private void setAdapter() {

        adapter = new SelectedServicesAdapter(selectedServices, getContext());
        adapter.setItemLongClick(this::onLongPress);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mBinding.selectedServices.setItemAnimator(new DefaultItemAnimator());
        mBinding.selectedServices.setLayoutManager(layoutManager);
        mBinding.selectedServices.setAdapter(adapter);

    }

    private void setPackageSpinner() {
        List<String> items = new ArrayList<>();
        for (Service service : user.getServices())
            items.add(service.getName());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.list_item, items);
        mBinding.serviceSpinner.setAdapter(adapter);
    }

    private void setSpinner() {
        List<String> items = new ArrayList<>();
        items.add("Hair");
        items.add("Beard");
        items.add("Skin Care");
        items.add("Massage");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.list_item, items);
        mBinding.type.setAdapter(adapter);
    }

    private void addPackage() {

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("users/vendor/" + user.getId());
        com.network.mysalon.models.Bundle bundle = new com.network.mysalon.models.Bundle();
        bundle.setId("salon-" + mBinding.serviceName.getText().toString() + "-" + user.getServices().size());
        bundle.setName(mBinding.serviceName.getText().toString());
        bundle.setDescription(mBinding.serviceDesc.getText().toString());
        bundle.setPrice(Double.parseDouble(mBinding.price.getText().toString()));
        bundle.setServiceIds(selectedServices);

        if (photoURI != null) {
            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            StorageReference imageRef = storageRef.child("Images/" + user.getId() + "/services/" + photoURI.getLastPathSegment());
            UploadTask uploadTask = imageRef.putFile(photoURI);

            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    d.dismiss();
                    return null;
                }

                return imageRef.getDownloadUrl();
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        bundle.setImage(downloadUri.toString());
                    }
                    user.getActivities().add(new Activities("Package Added", "You added a new Service - " + bundle.getName(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                    user.getBundles().add(bundle);
                    new UserPreferences(getContext()).setUser(user);
                    ref.setValue(user);
                    clearFields();
                    d.dismiss();
                    Toast.makeText(getContext(), "Package added", Toast.LENGTH_SHORT).show();
//                    getActivity().onBackPressed();
                }
            });
        } else {
            user.getActivities().add(new Activities("Package Added", "You added a new Service - " + bundle.getName(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
            user.getBundles().add(bundle);
            new UserPreferences(getContext()).setUser(user);
            ref.setValue(user);
            clearFields();
            d.dismiss();
            Toast.makeText(getContext(), "Package added", Toast.LENGTH_SHORT).show();
//            getActivity().onBackPressed();
        }

    }

    private void addService() {

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("users/vendor/" + user.getId());
        Service service = new Service();
        service.setId("salon-" + mBinding.serviceName.getText().toString() + "-" + user.getServices().size());
        service.setName(mBinding.serviceName.getText().toString());
        service.setDescription(mBinding.serviceDesc.getText().toString());
        service.setDuration(mBinding.duration.getText().toString() + " Mins");
        service.setPrice(Double.parseDouble(mBinding.price.getText().toString()));
        service.setType(mBinding.type.getText().toString());

        if (photoURI != null) {
            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            StorageReference imageRef = storageRef.child("Images/" + user.getId() + "/services/" + photoURI.getLastPathSegment());
            UploadTask uploadTask = imageRef.putFile(photoURI);

            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    d.dismiss();
                    return null;
                }

                return imageRef.getDownloadUrl();
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        service.setImage(downloadUri.toString());
                    }
                    user.getActivities().add(new Activities("Service Added", "You added a new Service - " + service.getName(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                    user.getServices().add(service);
                    new UserPreferences(getContext()).setUser(user);
                    ref.setValue(user);
                    clearFields();
                    d.dismiss();
                    Toast.makeText(getContext(), "Service added", Toast.LENGTH_SHORT).show();
//                    getActivity().onBackPressed();
                }
            });
        } else {
            user.getActivities().add(new Activities("Service Added", "You added a new Service - " + service.getName(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
            user.getServices().add(service);
            new UserPreferences(getContext()).setUser(user);
            ref.setValue(user);
            clearFields();
            d.dismiss();
            Toast.makeText(getContext(), "Service added", Toast.LENGTH_SHORT).show();
//            getActivity().onBackPressed();
        }
    }

    @Override
    public void onLongPress(int position) {

        SweetAlertDialogs.getInstance().showDialogYesNo(getContext(), "Remove Item?", "Are you sure, you want to remove?", new SweetAlertDialogs.OnDialogClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                selectedServices.remove(position);
                adapter.notifyDataSetChanged();
                sweetAlertDialog.dismissWithAnimation();
            }
        }, new SweetAlertDialogs.OnDialogClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        }, false);

    }

    private void clearFields() {

        mBinding.image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.logo));
        mBinding.serviceName.setText("");
        mBinding.type.setText("");
        mBinding.serviceDesc.setText("");
        mBinding.duration.setText("");
        mBinding.price.setText("");
        if (serviceType.equals("package")) {
            mBinding.serviceSpinner.setText("");
            selectedServices.clear();
            adapter.notifyDataSetChanged();
        }

    }

    @SuppressLint("IntentReset")
    public void showPictureDialog() {

        final Dialog d = new Dialog(getContext());
        d.setContentView(R.layout.dialog_capture);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(d.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        d.show();
        d.getWindow().setAttributes(layoutParams);

        TextView btnCamera, btnGallery;
        btnCamera = d.findViewById(R.id.btnCamera);
        btnGallery = d.findViewById(R.id.btnGallery);

        btnCamera.setOnClickListener(v -> {
            if (checkCameraPermission()) {
                openCamera();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        2);
            }
            d.dismiss();
        });

        btnGallery.setOnClickListener(v -> {
            if (checkReadPermission() && checkWritePermission()) {
                openGallery();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
            d.dismiss();
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_IMAGE_CODE) {
                photoURI = data.getData();

                imagePath = photoURI.getPath();
                Cursor cursor = null;
                try {
                    String[] proj = {MediaStore.Images.Media.DATA};
                    cursor = getActivity().getContentResolver().query(photoURI, proj, null, null, null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    imagePath = cursor.getString(column_index);
                } catch (Exception e) {
                    imagePath = photoURI.getPath();
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
                photoURI = FileProvider.getUriForFile(getContext(), getActivity().getApplicationContext().getPackageName() + ".fileprovider", new File(imagePath));
                mBinding.image.setImageURI(photoURI);
            } else if (requestCode == CAMERA_PIC_REQUEST_CODE) {
                photoURI = FileProvider.getUriForFile(getContext(), getActivity().getApplicationContext().getPackageName() + ".fileprovider", file);
                mBinding.image.setImageURI(photoURI);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            }
        } else if (requestCode == 2) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera();
            }
        }
    }

    private void openGallery() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");
        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE_CODE);
    }

    private void openCamera() {
        Intent m_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        Uri uri = FileProvider.getUriForFile(getContext(), getActivity().getApplicationContext().getPackageName() + ".fileprovider", file);
        m_intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(m_intent, CAMERA_PIC_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        String permission = Manifest.permission.CAMERA;
        int res = getActivity().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkReadPermission() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        int res = getActivity().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkWritePermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getActivity().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

}