package com.network.mysalon.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.network.mysalon.R;
import com.network.mysalon.adapters.ActivitiesAdapter;
import com.network.mysalon.databinding.FragmentActivitiesBinding;
import com.network.mysalon.models.Activities;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.UserPreferences;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class ActivitiesFragment extends Fragment implements Serializable {

    FragmentActivitiesBinding mBinding;
    ArrayList<Activities> activities = new ArrayList<>();
    ActivitiesAdapter activitiesAdapter = null;
    UserModel user;

    public ActivitiesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_activities, container, false);

        user = new UserPreferences(getContext()).getUser();
        setAdapter();

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        return mBinding.getRoot();
    }

    private void setAdapter() {
        FirebaseDatabase.getInstance().getReference().child("users/" + user.getRole() + "/" + user.getId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                activities.clear();
                UserModel userModel = snapshot.getValue(UserModel.class);
                user = userModel;

                if (userModel.getActivities() != null) {

                    mBinding.disclaimer.setVisibility(View.GONE);
                    mBinding.activitiesList.setVisibility(View.VISIBLE);

                    activities.addAll(userModel.getActivities());
                    Collections.sort(activities, Activities.sortDesc);
                    activitiesAdapter = new ActivitiesAdapter(activities, getContext());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    mBinding.activitiesList.setItemAnimator(new DefaultItemAnimator());
                    mBinding.activitiesList.setLayoutManager(layoutManager);
                    mBinding.activitiesList.setAdapter(activitiesAdapter);
                } else {
                    mBinding.disclaimer.setVisibility(View.VISIBLE);
                    mBinding.activitiesList.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}