package com.network.mysalon.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.badge.BadgeDrawable;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.network.mysalon.activities.salon.VendorDashboardActivity;
import com.network.mysalon.activities.user.UserDashboardActivity;
import com.network.mysalon.adapters.NotificationAdapter;
import com.network.mysalon.R;
import com.network.mysalon.databinding.FragmentNotificationBinding;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.models.Notifications;
import com.network.mysalon.models.UserModel;

import java.util.ArrayList;
import java.util.Collections;

public class NotificationFragment extends Fragment implements NotificationAdapter.OpenNotification {

    FragmentNotificationBinding mBinding;
    ArrayList<Notifications> notifications = new ArrayList<>();
    NotificationAdapter notificationAdapter = null;
    UserModel user;

    public NotificationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);

        user = new UserPreferences(getContext()).getUser();
        setAdapter();

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        mBinding.btnReadAll.setOnClickListener(v -> {
            for (int i = 0; i < user.getNotifications().size(); i++) {
                user.getNotifications().get(i).setNotificationRead(true);
                notifications.get(i).setNotificationRead(true);
            }
            notificationAdapter.notifyDataSetChanged();
            new UserPreferences(getContext()).setUser(user);
            FirebaseDatabase.getInstance().getReference().child("users/" + user.getRole() + "/" + user.getId()).setValue(user);

            BadgeDrawable badgeDrawable;
            if (user.getRole().equals("user"))
                badgeDrawable = ((UserDashboardActivity) getActivity()).getBadge();
            else
                badgeDrawable = ((VendorDashboardActivity) getActivity()).getBadge();
            badgeDrawable.setNumber(0);
            badgeDrawable.setVisible(false);

        });

        return mBinding.getRoot();
    }

    private void setAdapter() {
        FirebaseDatabase.getInstance().getReference().child("users/" + user.getRole() + "/" + user.getId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                notifications.clear();
                UserModel userModel = snapshot.getValue(UserModel.class);
                user = userModel;
                notifications.addAll(userModel.getNotifications());

                if (notifications.size() == 0) {
                    mBinding.notifications.setVisibility(View.GONE);
                    mBinding.noNotification.setVisibility(View.VISIBLE);
                } else {

                    mBinding.notifications.setVisibility(View.VISIBLE);
                    mBinding.noNotification.setVisibility(View.GONE);

                    Collections.sort(notifications, Notifications.sortDesc);
                    notificationAdapter = new NotificationAdapter(notifications, getContext());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    notificationAdapter.setOpenNotification(NotificationFragment.this);
                    mBinding.notifications.setItemAnimator(new DefaultItemAnimator());
                    mBinding.notifications.setLayoutManager(layoutManager);
                    mBinding.notifications.setAdapter(notificationAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void readNotification(Notifications notification) {

        for (int i = 0; i < user.getNotifications().size(); i++) {
            if (user.getNotifications().get(i).getNotificationId().equals(notification.getNotificationId())) {
                user.getNotifications().get(i).setNotificationRead(true);
                new UserPreferences(getContext()).setUser(user);
                break;
            }
        }
        FirebaseDatabase.getInstance().getReference().child("users/" + user.getRole() + "/" + user.getId()).setValue(user);

        if (user.getRole().equals("user"))
            ((UserDashboardActivity) getActivity()).refreshBadge();
        else
            ((VendorDashboardActivity) getActivity()).refreshBadge();

    }
}