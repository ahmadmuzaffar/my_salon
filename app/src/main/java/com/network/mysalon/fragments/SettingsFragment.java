package com.network.mysalon.fragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.network.mysalon.R;
import com.network.mysalon.activities.MenuActivity;
import com.network.mysalon.databinding.FragmentSettingsBinding;

import java.io.Serializable;

public class SettingsFragment extends Fragment implements Serializable {

    FragmentSettingsBinding mBinding;

    public SettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);

        clickListeners();

        return mBinding.getRoot();
    }

    private void clickListeners() {

        mBinding.notificationCard.setOnClickListener(v -> {

            if (mBinding.notificationExpandedLayout.getVisibility() == View.GONE) {
                mBinding.notificationCard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications, 0, R.drawable.ic_up, 0);
                mBinding.notificationExpandedLayout.setVisibility(View.VISIBLE);
            } else {
                mBinding.notificationCard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications, 0, R.drawable.ic_down, 0);
                mBinding.notificationExpandedLayout.setVisibility(View.GONE);
            }

        });

        mBinding.loginHistoryCard.setOnClickListener(v -> {

            ((MenuActivity) getActivity()).openLoginHistory();

        });

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

    }
}