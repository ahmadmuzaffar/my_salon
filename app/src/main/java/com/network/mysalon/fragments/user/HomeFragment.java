package com.network.mysalon.fragments.user;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.network.mysalon.R;
import com.network.mysalon.activities.MapsActivity;
import com.network.mysalon.activities.MenuActivity;
import com.network.mysalon.activities.salon.AllSalonsActivity;
import com.network.mysalon.activities.salon.VendorRegisterActivity;
import com.network.mysalon.adapters.SalonAdapter;
import com.network.mysalon.databinding.FragmentHomeBinding;
import com.network.mysalon.fragments.salon.SalonDetailFragment;
import com.network.mysalon.models.UserModel;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import cn.bingerz.android.fastlocation.FastLocation;
import cn.bingerz.android.fastlocation.LocationResultListener;

import static android.content.Context.LOCATION_SERVICE;

public class HomeFragment extends Fragment implements SalonAdapter.ItemClickListener {

    FragmentHomeBinding mBinding;
    ArrayList<UserModel> salonItems = new ArrayList<>();
    SalonAdapter salonAdapter = null;
    FastLocation fastLocation;
    private LocationManager mLocationManager;

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);

        mBinding.btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (salonItems.size() == 0) {
                    Toast.makeText(getContext(), "No Salon to show", Toast.LENGTH_SHORT).show();
                    return;
                }

                mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                fastLocation = new FastLocation(getContext());

                boolean gps_enabled = false;

                try {
                    gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                if (!gps_enabled) {
                    new AlertDialog.Builder(getContext())
                            .setMessage("Please turn on location")
                            .setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                            .setNegativeButton("Cancel", null)
                            .show();
                } else {

                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getContext(), "Permission not granted", Toast.LENGTH_SHORT).show();
                    } else {
                        fastLocation.getLocation(new LocationResultListener() {
                            @Override
                            public void onResult(Location location) {
                                startActivity(new Intent(getActivity(), MapsActivity.class)
                                        .putExtra("lat", String.valueOf(location.getLatitude()))
                                        .putExtra("long", String.valueOf(location.getLongitude())));
                            }
                        });
                    }
                }
            }
        });
        mBinding.viewAll.setOnClickListener(v -> {
            if (salonItems.size() == 0) {
                Toast.makeText(getContext(), "No Salon to show", Toast.LENGTH_SHORT).show();
                return;
            }
            startActivity(new Intent(getActivity(), AllSalonsActivity.class));
        });

        mBinding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setAdapter();
                mBinding.swipe.setRefreshing(false);
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        setAdapter();
    }

    private void setAdapter() {

        FirebaseDatabase.getInstance().getReference().child("users/vendor").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                salonItems.clear();
                for (DataSnapshot userSnapshot : snapshot.getChildren()) {
                    UserModel userModel = userSnapshot.getValue(UserModel.class);
                    salonItems.add(userModel);
                }

                if (salonItems.size() == 0) {
                    mBinding.salonsList.setVisibility(View.GONE);
                    mBinding.noSalon.setVisibility(View.VISIBLE);
                } else {

                    mBinding.salonsList.setVisibility(View.VISIBLE);
                    mBinding.noSalon.setVisibility(View.GONE);

                    Collections.sort(salonItems, UserModel.rate);

                    ArrayList<UserModel> temp = new ArrayList<>();
                    for (int i = 0; i < salonItems.size(); i++) {
                        if (i < 4)
                            temp.add(salonItems.get(i));
                        else
                            break;
                    }

                    salonAdapter = new SalonAdapter(temp, getContext());
                    salonAdapter.setItemClickListener(HomeFragment.this);
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false);
                    mBinding.salonsList.setItemAnimator(new DefaultItemAnimator());
                    mBinding.salonsList.setLayoutManager(mLayoutManager);
                    mBinding.salonsList.setAdapter(salonAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

    }

    @Override
    public void onClick(int position) {
        Fragment mFragment = new SalonDetailFragment();

        startActivity(new Intent(getContext(), MenuActivity.class)
                .putExtra("fragment", (Serializable) mFragment)
                .putExtra("user", salonItems.get(position)));

    }
}