package com.network.mysalon.fragments.salon;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.FirebaseDatabase;
import com.network.mysalon.R;
import com.network.mysalon.activities.PreviewActivity;
import com.network.mysalon.adapters.AllBundlesAdapter;
import com.network.mysalon.adapters.AllServicesAdapter;
import com.network.mysalon.databinding.FragmentSalonDetailBinding;
import com.network.mysalon.models.Activities;
import com.network.mysalon.models.Booking;
import com.network.mysalon.models.Service;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.FcmNotifier;
import com.network.mysalon.utilities.UserPreferences;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SalonDetailFragment extends Fragment implements Serializable, AllServicesAdapter.ClickListener, AllBundlesAdapter.ClickListener {

    FragmentSalonDetailBinding mBinding;
    UserModel salon;
    UserModel user;
    AllServicesAdapter allServicesAdapter;
    AllBundlesAdapter allBundlesAdapter;

    public SalonDetailFragment() {
    }

    @Override
    public void onResume() {

        setAdapters();

        super.onResume();
    }

    private void setAdapters() {

        if (salon.getServices().size() == 0 && salon.getBundles().size() == 0) {
            mBinding.both.setVisibility(View.GONE);
            mBinding.noService.setVisibility(View.VISIBLE);
        } else {

            if (salon.getServices().size() == 0) {
                mBinding.services.setVisibility(View.GONE);
                mBinding.allServices.setVisibility(View.GONE);
            } else {
                mBinding.services.setVisibility(View.VISIBLE);
                mBinding.allServices.setVisibility(View.VISIBLE);
                List<Service> list = new ArrayList<>(salon.getServices());

                allServicesAdapter = new AllServicesAdapter(list, getContext());
                allServicesAdapter.setListener(null);
                allServicesAdapter.setClickListener(SalonDetailFragment.this);
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                mBinding.services.setItemAnimator(new DefaultItemAnimator());
                mBinding.services.setLayoutManager(mLayoutManager);
                mBinding.services.setAdapter(allServicesAdapter);
            }

            if (salon.getBundles().size() == 0) {
                mBinding.bundles.setVisibility(View.GONE);
                mBinding.packages.setVisibility(View.GONE);
            } else {
                mBinding.bundles.setVisibility(View.VISIBLE);
                mBinding.packages.setVisibility(View.VISIBLE);
                List<com.network.mysalon.models.Bundle> list = new ArrayList<>(salon.getBundles());

                allBundlesAdapter = new AllBundlesAdapter(list, getContext());
                allBundlesAdapter.setListener(null);
                allBundlesAdapter.setClickListener(SalonDetailFragment.this);
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                mBinding.bundles.setItemAnimator(new DefaultItemAnimator());
                mBinding.bundles.setLayoutManager(mLayoutManager);
                mBinding.bundles.setAdapter(allBundlesAdapter);
            }
        }

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_salon_detail, container, false);
        user = new UserPreferences(getContext()).getUser();
        salon = (UserModel) getArguments().getSerializable("user");

        populateFields();
        clickListeners();

        return mBinding.getRoot();
    }

    private void clickListeners() {

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        mBinding.profilePicture.setOnClickListener(v -> {
            if (salon.getProfileImage() != null)
                startActivity(new Intent(getActivity(), PreviewActivity.class).putExtra("imageUrl", salon.getProfileImage()));
        });

        mBinding.ratingLayout.setOnClickListener(v -> {
            if (salon.getReviewGivers().size() > 0) {
                for (String reviewGiver : salon.getReviewGivers())
                    if (reviewGiver.contains(user.getId())) {
                        Toast.makeText(getContext(), "Review already given", Toast.LENGTH_SHORT).show();
                        return;
                    }
            }

            showReviewDialog(salon);
        });

        mBinding.btnLike.setOnClickListener(v -> {
            if (salon.getLikeGivers().size() > 0) {
                if (salon.getLikeGivers().contains(user.getId())) {
                    salon.getLikeGivers().remove(user.getId());

                    mBinding.btnLike.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_like_outline));
                    user.getActivities().add(new Activities("Like", "You dis-liked " + salon.getNameOnBoard() + "'s profile", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                    FcmNotifier.sendNotification(user.getFullName() + " disliked your profile", "Like Notification", user, salon);
                } else {
                    salon.getLikeGivers().add(user.getId());
                    mBinding.btnLike.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_like_filled));
                    user.getActivities().add(new Activities("Like", "You liked " + salon.getNameOnBoard() + "'s profile", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                    FcmNotifier.sendNotification(user.getFullName() + " liked your profile", "Like Notification", user, salon);
                }
            } else {
                salon.getLikeGivers().add(user.getId());
                mBinding.btnLike.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_like_filled));
                user.getActivities().add(new Activities("Like", "You liked " + salon.getNameOnBoard() + "'s profile", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                FcmNotifier.sendNotification(user.getFullName() + " liked your profile", "Like Notification", user, salon);
            }
            mBinding.likes.setText("(" + salon.getLikeGivers().size() + ")");
            FirebaseDatabase.getInstance().getReference("users/" + salon.getRole() + "/" + salon.getId()).setValue(salon);
            FirebaseDatabase.getInstance().getReference("users/" + user.getRole() + "/" + user.getId()).setValue(user);

            new UserPreferences(getContext()).setUser(user);
        });

    }

    private void populateFields() {

        if (salon.getProfileImage() != null)
            Glide.with(getContext()).load(salon.getProfileImage()).into(mBinding.profilePicture);
        mBinding.title.setText(salon.getNameOnBoard());

        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(salon.getLatitude()), Double.parseDouble(salon.getLongitude()), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        mBinding.address.setText(addresses.get(0).getAddressLine(0));
        if (salon.getReviewGivers().size() > 0) {
            float rating = Float.parseFloat(salon.getRating()) / (float) salon.getReviewGivers().size();
            mBinding.ratingBar.setRating(rating);
            mBinding.rating.setText(rating + "");
        } else {
            mBinding.ratingBar.setRating(0f);
            mBinding.rating.setText("0");
        }
        mBinding.likes.setText("(" + salon.getLikeGivers().size() + ")");
        if (salon.getLikeGivers().contains(user.getId())) {
            mBinding.btnLike.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_like_filled));
        }

    }

    @Override
    public void onAddToCartClick(Service service) {

        Booking booking = new UserPreferences(getContext()).getCartData();
        if (booking == null)
            booking = new Booking();
        if (salon.getServices().contains(service)) {
            booking.getServices().add(service);
            booking.setUser(new UserPreferences(getContext()).getUser());
            booking.setSalon(salon);
            new UserPreferences(getContext()).setCartData(booking);
            Toast.makeText(getContext(), "Added to cart!", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getContext(), "Booking contains services from the same Salon.\nKindly clear the cart to select from other salon", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(Service service) {

    }

    @Override
    public void onAddToCartClick(com.network.mysalon.models.Bundle bundle) {

        Booking booking = new UserPreferences(getContext()).getCartData();
        if (booking == null)
            booking = new Booking();
        if (salon.getBundles().contains(bundle)) {
            booking.getBundles().add(bundle);
            booking.setUser(new UserPreferences(getContext()).getUser());
            booking.setSalon(salon);
            new UserPreferences(getContext()).setCartData(booking);
            Toast.makeText(getContext(), "Added to cart!", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getContext(), "Booking contains packages from the same Salon.\nKindly clear the cart to select from other salon", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(com.network.mysalon.models.Bundle bundle) {

    }

    @SuppressLint("ClickableViewAccessibility")
    private void showReviewDialog(UserModel obj) {
        final Dialog d = new Dialog(getContext());
        d.setContentView(R.layout.dialog_rating);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView btnGiveReview, btnCancel;
        EditText reviewValue;
        RatingBar ratingBar;

        btnGiveReview = d.findViewById(R.id.btnGiveReview);
        btnCancel = d.findViewById(R.id.btnCancel);
        reviewValue = d.findViewById(R.id.reviewValue);
        ratingBar = d.findViewById(R.id.ratingBar);

        ratingBar.setOnTouchListener(new View.OnTouchListener() {
            private float downXValue;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    downXValue = event.getX();
                    return false;
                }

                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    // When true is returned, view will not handle this event.
                    return true;
                }

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    float currentX = event.getX();
                    float difference = 0;
                    // Swipe on left side
                    if (currentX < downXValue)
                        difference = downXValue - currentX;
                        // Swipe on right side
                    else if (currentX > downXValue)
                        difference = currentX - downXValue;

                    if (difference < 10)
                        return false;

                    return true;
                }
                return false;
            }
        });

        reviewValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    ratingBar.setRating(Float.parseFloat(s.toString()));
                }
            }
        });

        btnGiveReview.setOnClickListener(v -> {
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            double rating = 0.0;
            if (obj.getRating() != null)
                rating = Double.parseDouble(obj.getRating());
            rating += ratingBar.getRating();

            obj.getReviewGivers().add(user.getId());
            obj.setRating(String.valueOf(rating));

            mBinding.ratingBar.setRating((float) rating);
            user.getActivities().add(new Activities("Rating", "You rated " + obj.getNameOnBoard() + "'s profile " + ratingBar.getRating() + "/5", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
            database.getReference().child("users/" + user.getRole() + "/" + user.getId()).setValue(user);
            database.getReference().child("users/" + obj.getRole() + "/" + obj.getId()).setValue(obj);
            d.dismiss();

        });

        btnCancel.setOnClickListener(v -> {
            d.dismiss();
        });

        d.show();
    }
}