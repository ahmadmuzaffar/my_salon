package com.network.mysalon.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.network.mysalon.R;
import com.network.mysalon.fragments.user.UserProfileFragment;
import com.network.mysalon.fragments.salon.SalonProfileFragment;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.activities.LoginActivity;
import com.network.mysalon.activities.MenuActivity;
import com.network.mysalon.databinding.FragmentMenuBinding;
import com.network.mysalon.models.UserModel;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MenuFragment extends Fragment {

    FragmentMenuBinding mBinding;
    UserModel userModel = null;

    public MenuFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false);

        userModel = new UserPreferences(getContext()).getUser();

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        mBinding.profileCard.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), MenuActivity.class);
            Fragment fragment = userModel.getRole().equals("user") ? new UserProfileFragment() : new SalonProfileFragment();
            intent.putExtra("fragment", (Serializable) fragment);
            startActivity(intent);
        });

        mBinding.activityCard.setOnClickListener(v -> {
            Fragment fragment = new ActivitiesFragment();
            startActivity(new Intent(getActivity(), MenuActivity.class)
                    .putExtra("fragment", (Serializable) fragment));
        });

        mBinding.settingCard.setOnClickListener(v -> {
            Fragment fragment = new SettingsFragment();
            startActivity(new Intent(getActivity(), MenuActivity.class)
                    .putExtra("fragment", (Serializable) fragment));
        });

        mBinding.termsPolicyCard.setOnClickListener(v -> {
            Uri uri = Uri.parse("https://www.termsandcondiitionssample.com/live.php?token=MqciyJCE1NmG07aT1hav2oFWMxq8oI2x");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        });

        mBinding.problemCard.setOnClickListener(v -> {
            Toast.makeText(getContext(), "Feature is disabled", Toast.LENGTH_SHORT).show();
        });

        mBinding.signOutCard.setOnClickListener(v -> {
            FirebaseAuth.getInstance().signOut();
            userModel.setFcmToken(null);
            String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date());
            userModel.getLoginHistory().get(userModel.getLoginHistory().size() - 1).setLogoutDateTime(date);
            FirebaseDatabase.getInstance().getReference("users/" + userModel.getRole() + "/" + userModel.getId()).setValue(userModel);
            new UserPreferences(getContext()).setUser(null);
            startActivity(new Intent(getActivity(), LoginActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        });

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        userModel = new UserPreferences(getContext()).getUser();
        if (userModel.getProfileImage() != null) {
            Glide.with(getContext())
                    .load(userModel.getProfileImage())
                    .into(mBinding.profilePicture);
        }
    }
}