package com.network.mysalon.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.network.mysalon.R;
import com.network.mysalon.activities.MenuActivity;
import com.network.mysalon.adapters.BookingsAdapter;
import com.network.mysalon.databinding.FragmentBookingBinding;
import com.network.mysalon.models.Booking;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.UserPreferences;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BookingFragment extends Fragment implements BookingsAdapter.ClickListener {

    FragmentBookingBinding mBinding;
    List<Booking> bookingList = new ArrayList<>();
    BookingsAdapter adapter;
    UserModel user;
    private List<Booking> tempList = new ArrayList<>();

    public BookingFragment() {
    }

    @Override
    public void onResume() {

        Booking booking = new UserPreferences(getContext()).getCartData();
        if (booking != null) {
            mBinding.cartBadge.setVisibility(View.VISIBLE);
        } else
            mBinding.cartBadge.setVisibility(View.GONE);
        setAdapter();

        super.onResume();
    }

    private void setAdapter() {

        FirebaseDatabase.getInstance().getReference("bookings").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                bookingList.clear();
                for (DataSnapshot bookings : snapshot.getChildren()) {
                    Booking booking = bookings.getValue(Booking.class);
                    if (user.getRole().toLowerCase().equals("user")) {
                        if (booking.getUser().getId().equals(user.getId())) {
                            bookingList.add(booking);
                        }
                    } else {
                        if (booking.getSalon().getId().equals(user.getId())) {
                            bookingList.add(booking);
                        }
                    }
                }

                tempList = new ArrayList<>();
                if (mBinding.category.getText().toString().equals("All")) {
                    tempList.addAll(bookingList);
                } else if (mBinding.category.getText().toString().equals("Active")) {
                    for (Booking booking : bookingList) {
                        if (booking.getStatus().equals(Booking.STATUS.ACTIVE)) {
                            tempList.add(booking);
                        }
                    }
                } else if (mBinding.category.getText().toString().equals("Requests")) {
                    for (Booking booking : bookingList) {
                        if (booking.getStatus().equals(Booking.STATUS.REQUEST)) {
                            tempList.add(booking);
                        }
                    }
                } else if (mBinding.category.getText().toString().equals("Completed")) {
                    for (Booking booking : bookingList) {
                        if (booking.getStatus().equals(Booking.STATUS.COMPLETED)) {
                            tempList.add(booking);
                        }
                    }
                } else if (mBinding.category.getText().toString().equals("Cancelled")) {
                    for (Booking booking : bookingList) {
                        if (booking.getStatus().equals(Booking.STATUS.CANCELLED)) {
                            tempList.add(booking);
                        }
                    }
                }

                setRecyclerView(tempList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("TAG", "onCancelled: ");
            }
        });

    }

    private void setRecyclerView(List<Booking> list) {

        if (list.size() == 0) {
            adapter = new BookingsAdapter(list, getContext(), this);
            mBinding.bookings.setVisibility(View.GONE);
            mBinding.noBookings.setVisibility(View.VISIBLE);
        } else {

            mBinding.bookings.setVisibility(View.VISIBLE);
            mBinding.noBookings.setVisibility(View.GONE);

            Collections.reverse(list);
            adapter = new BookingsAdapter(list, getContext(), this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            mBinding.bookings.setItemAnimator(new DefaultItemAnimator());
            mBinding.bookings.setLayoutManager(layoutManager);
            mBinding.bookings.setAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_booking, container, false);
        user = new UserPreferences(getContext()).getUser();
        if (user.getRole().equals("user")) {
            mBinding.cartLayout.setVisibility(View.VISIBLE);
        }

        mBinding.cartLayout.setOnClickListener(v -> {
            if (mBinding.cartBadge.getVisibility() == View.VISIBLE) {
                Fragment fragment = new CartFragment();
                startActivity(new Intent(getActivity(), MenuActivity.class)
                        .putExtra("fragment", (Serializable) fragment));
            } else
                Toast.makeText(getContext(), "Cart is empty.", Toast.LENGTH_SHORT).show();
        });
        setRecyclerView(tempList);

        mBinding.searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    adapter.getBookings().clear();
                    adapter.getBookings().addAll(bookingList);
                    adapter.notifyDataSetChanged();
                } else {
                    String text = mBinding.searchView.getText().toString().toLowerCase();
                    adapter.getBookings().clear();
                    for (Booking booking : bookingList) {
                        if (booking.getUser().getFullName().toLowerCase().contains(text) ||
                                booking.getId().contains(text) ||
                                booking.getScheduledDateTime().contains(text) ||
                                booking.getDateTime().contains(text) ||
                                booking.getStatus().toString().contains(text) ||
                                String.valueOf(booking.getTotalPayment()).contains(text)
                        ) {
                            adapter.getBookings().add(booking);
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        });

        mBinding.filter.setOnClickListener(v -> {
            showDialog();
        });

        return mBinding.getRoot();
    }


    @SuppressLint("ClickableViewAccessibility")
    private void showDialog() {
        final Dialog d = new Dialog(getContext());
        d.setContentView(R.layout.dialog_filter_bookings);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RadioButton all, active, request, completed, cancelled;
        RadioGroup category;

        all = d.findViewById(R.id.all);
        active = d.findViewById(R.id.active);
        request = d.findViewById(R.id.request);
        completed = d.findViewById(R.id.completed);
        cancelled = d.findViewById(R.id.cancelled);
        category = d.findViewById(R.id.category);

        if (mBinding.category.getText().equals("All"))
            all.setChecked(true);
        else if (mBinding.category.getText().equals("Active"))
            active.setChecked(true);
        else if (mBinding.category.getText().equals("Requests"))
            request.setChecked(true);
        else if (mBinding.category.getText().equals("Completed"))
            completed.setChecked(true);
        else
            cancelled.setChecked(true);

        category.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                tempList.clear();
                if (i == R.id.all) {
                    mBinding.category.setText("All");
                    tempList.addAll(bookingList);
                    setRecyclerView(tempList);
                    d.dismiss();
                } else if (i == R.id.active) {
                    mBinding.category.setText("Active");
                    for (Booking booking : bookingList) {
                        if (booking.getStatus().equals(Booking.STATUS.ACTIVE)) {
                            tempList.add(booking);
                        }
                    }
                    setRecyclerView(tempList);
                    d.dismiss();
                } else if (i == R.id.request) {
                    mBinding.category.setText("Requests");
                    for (Booking booking : bookingList) {
                        if (booking.getStatus().equals(Booking.STATUS.REQUEST)) {
                            tempList.add(booking);
                        }
                    }
                    setRecyclerView(tempList);
                    d.dismiss();
                } else if (i == R.id.completed) {
                    mBinding.category.setText("Completed");
                    for (Booking booking : bookingList) {
                        if (booking.getStatus().equals(Booking.STATUS.COMPLETED)) {
                            tempList.add(booking);
                        }
                    }
                    setRecyclerView(tempList);
                    d.dismiss();
                } else {
                    mBinding.category.setText("Cancelled");
                    for (Booking booking : bookingList) {
                        if (booking.getStatus().equals(Booking.STATUS.CANCELLED)) {
                            tempList.add(booking);
                        }
                    }
                    setRecyclerView(tempList);
                    d.dismiss();
                }
            }
        });

        d.show();
    }

    @Override
    public void onClick(int pos) {
        Fragment fragment = new BookingDetailFragment();
        startActivity(new Intent(getActivity(), MenuActivity.class)
                .putExtra("fragment", (Serializable) fragment)
                .putExtra("booking", adapter.getBookings().get(pos)));
    }
}