package com.network.mysalon.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.network.mysalon.R;
import com.network.mysalon.adapters.LoginHistoryAdapter;
import com.network.mysalon.databinding.FragmentLoginHistoryBinding;
import com.network.mysalon.models.LoginHistory;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.UserPreferences;

import java.util.ArrayList;
import java.util.Collections;

public class LoginHistoryFragment extends Fragment {

    FragmentLoginHistoryBinding mBinding;
    ArrayList<LoginHistory> loginHistories = new ArrayList<>();
    LoginHistoryAdapter loginHistoryAdapter = null;
    UserModel user;

    public LoginHistoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_history, container, false);

        user = new UserPreferences(getContext()).getUser();
        setAdapter();

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        return mBinding.getRoot();
    }

    private void setAdapter() {

        FirebaseDatabase.getInstance().getReference().child("users/" + user.getRole() + "/" + user.getId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                loginHistories.clear();
                UserModel userModel = snapshot.getValue(UserModel.class);
                user = userModel;
                loginHistories.addAll(userModel.getLoginHistory());
                Collections.reverse(loginHistories);
                loginHistoryAdapter = new LoginHistoryAdapter(loginHistories, getContext());
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                mBinding.sessions.setItemAnimator(new DefaultItemAnimator());
                mBinding.sessions.setLayoutManager(layoutManager);
                mBinding.sessions.setAdapter(loginHistoryAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

    }
}