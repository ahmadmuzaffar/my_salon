package com.network.mysalon.fragments.salon;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.network.mysalon.R;
import com.network.mysalon.activities.MenuActivity;
import com.network.mysalon.adapters.AllBundlesAdapter;
import com.network.mysalon.adapters.AllServicesAdapter;
import com.network.mysalon.databinding.FragmentServiceListBinding;
import com.network.mysalon.models.Booking;
import com.network.mysalon.models.Service;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.UserPreferences;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ServiceListFragment extends Fragment implements Serializable, AllServicesAdapter.ClickListener, AllBundlesAdapter.ClickListener {

    FragmentServiceListBinding mBinding;
    String serviceType;
    AllServicesAdapter allServicesAdapter;
    AllBundlesAdapter allBundlesAdapter;
    List<Service> services = new ArrayList<>();
    List<com.network.mysalon.models.Bundle> bundles = new ArrayList<>();
    UserModel user;

    public ServiceListFragment() {
    }

    @Override
    public void onResume() {

        setAdapter();
        super.onResume();
    }

    private void setAdapter() {

        if (serviceType.equals("service")) {

            if (user.getServices().size() == 0) {
                mBinding.noService.setText("No Service");
                mBinding.noServices.setVisibility(View.VISIBLE);
                mBinding.list.setVisibility(View.GONE);
                return;
            } else {
                mBinding.noServices.setVisibility(View.GONE);
                mBinding.list.setVisibility(View.VISIBLE);
            }

            services = user.getServices();
            allServicesAdapter = new AllServicesAdapter(services, getContext());
            allServicesAdapter.setListener(null);
            allServicesAdapter.setClickListener(ServiceListFragment.this);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mBinding.list.setItemAnimator(new DefaultItemAnimator());
            mBinding.list.setLayoutManager(new GridLayoutManager(getContext(), 2));
            mBinding.list.setAdapter(allServicesAdapter);
        } else {

            if (user.getBundles().size() == 0) {
                mBinding.noService.setText("No Package");
                mBinding.noServices.setVisibility(View.VISIBLE);
                mBinding.list.setVisibility(View.GONE);
                return;
            } else {
                mBinding.noServices.setVisibility(View.GONE);
                mBinding.list.setVisibility(View.VISIBLE);
            }

            RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            relativeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            relativeParams.addRule(RelativeLayout.BELOW, R.id.header);

            mBinding.list.setLayoutParams(relativeParams);
            bundles = user.getBundles();
            allBundlesAdapter = new AllBundlesAdapter(bundles, getContext());
            allBundlesAdapter.setListener(null);
            allBundlesAdapter.setClickListener(ServiceListFragment.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mBinding.list.setItemAnimator(new DefaultItemAnimator());
            mBinding.list.setLayoutManager(mLayoutManager);
            mBinding.list.setAdapter(allBundlesAdapter);
        }

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_service_list, container, false);

        user = new UserPreferences(getContext()).getUser();
        Bundle bundle = getArguments();
        serviceType = bundle.getString("service_type");
        if (serviceType.equals("package")) {
            mBinding.header.setText(getString(R.string.all_packages));
            mBinding.btnAdd.setText(getString(R.string.add_package));
        }

        mBinding.btnAdd.setOnClickListener(v -> {
            ((MenuActivity) getActivity()).openAddService();
        });

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        return mBinding.getRoot();
    }

    @Override
    public void onAddToCartClick(Service service) {

        Booking booking = new UserPreferences(getContext()).getCartData();
        if (booking == null)
            booking = new Booking();
        if (user.getServices().contains(service)) {
            booking.getServices().add(service);
            booking.setUser(new UserPreferences(getContext()).getUser());
            booking.setSalon(user);
            new UserPreferences(getContext()).setCartData(booking);
            Toast.makeText(getContext(), "Added to cart!", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getContext(), "Booking contains services from the same Salon.\nKindly clear the cart to select from other salon", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(Service service) {
        Fragment mFragment = new AddServiceFragment();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putSerializable("service", service);
        bundle.putString("action", "view");
        bundle.putString("service_type", "service");
        mFragment.setArguments(bundle);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.add(R.id.fragment, mFragment, "service_fragment");
        transaction.commit();
    }

    @Override
    public void onAddToCartClick(com.network.mysalon.models.Bundle bundle) {

        Booking booking = new UserPreferences(getContext()).getCartData();
        if (booking == null)
            booking = new Booking();
        if (user.getBundles().contains(bundle)) {
            booking.getBundles().add(bundle);
            booking.setUser(new UserPreferences(getContext()).getUser());
            booking.setSalon(user);
            new UserPreferences(getContext()).setCartData(booking);
            Toast.makeText(getContext(), "Added to cart!", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getContext(), "Booking contains packages from the same Salon.\nKindly clear the cart to select from other salon", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(com.network.mysalon.models.Bundle bundle) {
        Fragment mFragment = new AddServiceFragment();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Bundle bundles = new Bundle();
        bundles.putSerializable("package", bundle);
        bundles.putString("action", "view");
        bundles.putString("service_type", "package");
        mFragment.setArguments(bundles);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.add(R.id.fragment, mFragment, "package_fragment");
        transaction.commit();
    }
}