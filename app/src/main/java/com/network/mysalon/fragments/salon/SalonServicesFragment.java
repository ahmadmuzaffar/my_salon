package com.network.mysalon.fragments.salon;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.network.mysalon.R;
import com.network.mysalon.activities.MenuActivity;
import com.network.mysalon.databinding.FragmentServicesBinding;

import java.io.Serializable;

public class SalonServicesFragment extends Fragment implements Serializable {

    FragmentServicesBinding mBinding;

    public SalonServicesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_services, container, false);

        clickListeneres();

        return mBinding.getRoot();
    }

    private void clickListeneres() {

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        mBinding.allServicesCard.setOnClickListener(v -> {

            Fragment fragment = new ServiceListFragment();
            startActivity(new Intent(getContext(), MenuActivity.class)
                    .putExtra("fragment", (Serializable) fragment)
                    .putExtra("service_type", "service"));

        });

        mBinding.packagesCard.setOnClickListener(v -> {

            Fragment fragment = new ServiceListFragment();
            startActivity(new Intent(getContext(), MenuActivity.class)
                    .putExtra("fragment", (Serializable) fragment)
                    .putExtra("service_type", "package"));

        });

//        mBinding.offersCard.setOnClickListener(v -> {
//
//
//        });

    }
}