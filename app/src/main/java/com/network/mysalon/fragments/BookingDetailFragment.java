package com.network.mysalon.fragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.bumptech.glide.Glide;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.firebase.database.FirebaseDatabase;
import com.network.mysalon.R;
import com.network.mysalon.adapters.CartAdapter;
import com.network.mysalon.databinding.FragmentBookingDetailBinding;
import com.network.mysalon.models.Activities;
import com.network.mysalon.models.Booking;
import com.network.mysalon.models.CartItem;
import com.network.mysalon.models.Service;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.FcmNotifier;
import com.network.mysalon.utilities.UserPreferences;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookingDetailFragment extends Fragment implements Serializable {

    private Booking booking;
    private FragmentBookingDetailBinding mBinding;
    CartAdapter adapter;
    List<CartItem> itemList = new ArrayList<>();
    private ArrayAdapter<String> arrayAdapter;
    UserModel user;

    public BookingDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_booking_detail, container, false);
        booking = getArguments().containsKey("booking") ? (Booking) getArguments().getSerializable("booking") : null;
        user = new UserPreferences(getContext()).getUser();

        if (booking.getSalon().getId().equals(user.getId()))
            setSpinner();
        populateData();

        if (booking.getSalon().getId().equals(user.getId())) {

            mBinding.dateTimeField.setOnClickListener(v -> {
                new SingleDateAndTimePickerDialog.Builder(getContext())
                        .bottomSheet()
                        .curved()
                        .displayHours(true)
                        .displayMinutes(true)
                        .displayAmPm(true)
                        .mustBeOnFuture()
                        .title("Select Date and Time")
                        .listener(date -> {
                            Date date1 = null;
                            try {
                                date1 = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy").parse(date.toString());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            mBinding.dateTimeField.setText(new SimpleDateFormat("dd MMM, yyyy - hh:mm a").format(date1));
                        }).display();
            });

            mBinding.updateButton.setOnClickListener(v -> {
                booking.setStatus(mBinding.statusSpinner.getText().toString().equals("REQUEST") ? Booking.STATUS.REQUEST :
                        mBinding.statusSpinner.getText().toString().equals("ACTIVE") ? Booking.STATUS.ACTIVE :
                                mBinding.statusSpinner.getText().toString().equals("CANCELLED") ? Booking.STATUS.CANCELLED :
                                        Booking.STATUS.COMPLETED);
                booking.setScheduledDateTime(mBinding.dateTimeField.getText().toString());

                FcmNotifier.sendNotification(booking.getSalon().getNameOnBoard() + " updated Booking '" + booking.getId() + "' details", "Booking", booking.getSalon(), booking.getUser());
                booking.getSalon().getActivities().add(new Activities("Booking", "You updated Booking '" + booking.getId() + "' details", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                FirebaseDatabase.getInstance().getReference("users/vendor/" + booking.getSalon().getId()).setValue(booking.getSalon());
                FirebaseDatabase.getInstance().getReference("bookings/" + booking.getId()).setValue(booking);
                getActivity().onBackPressed();

            });

        } else {
            mBinding.updateCard.setVisibility(View.GONE);
            mBinding.statusSpinner.setFocusable(false);
            mBinding.statusSpinner.setClickable(false);
            mBinding.statusSpinner.setCursorVisible(false);

            mBinding.toggle.setText("Salon Details");
            if (booking.getSalon().getProfileImage() != null) {
                Glide.with(getActivity()).load(booking.getSalon().getProfileImage()).into(mBinding.dpUser);
            }
            mBinding.userName.setText(booking.getSalon().getFullName());
            mBinding.email.setText(booking.getSalon().getEmailAddress());
            mBinding.phone.setText(booking.getSalon().getPhoneNumber());

        }

        mBinding.back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        return mBinding.getRoot();
    }

    private void populateData() {

        mBinding.id.setText(booking.getId());

        if (booking.getSalon().getId().equals(user.getId())) {
            if (booking.getUser().getProfileImage() != null) {
                Glide.with(getActivity()).load(booking.getUser().getProfileImage()).into(mBinding.dpUser);
            }
            mBinding.userName.setText(booking.getUser().getFullName());
            mBinding.email.setText(booking.getUser().getEmailAddress());
            mBinding.phone.setText(booking.getUser().getPhoneNumber());
        }
        mBinding.statusSpinner.setText(booking.getStatus().toString(), false);
        mBinding.dateTimeField.setText(booking.getScheduledDateTime());

        setCartAdapter();

    }

    private void setCartAdapter() {

        for (Service service : booking.getServices()) {
            CartItem cartItem = new CartItem(service.getName(), "service", service.getPrice());
            itemList.add(cartItem);
        }
        for (com.network.mysalon.models.Bundle bundle : booking.getBundles()) {
            CartItem cartItem = new CartItem(bundle.getName(), "bundle", bundle.getPrice());
            itemList.add(cartItem);
        }

        adapter = new CartAdapter(itemList, getContext());
        adapter.setHideRemove(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mBinding.cartItems.setItemAnimator(new DefaultItemAnimator());
        mBinding.cartItems.setLayoutManager(layoutManager);
        mBinding.cartItems.setAdapter(adapter);

    }

    private void setSpinner() {
        List<String> items = new ArrayList<>();
        items.add("REQUEST");
        items.add("ACTIVE");
        items.add("CANCELLED");
        items.add("COMPLETED");
        arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.list_item, items);
        mBinding.statusSpinner.setAdapter(arrayAdapter);
    }
}