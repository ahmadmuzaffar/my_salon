package com.network.mysalon.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class UserModel implements Serializable {

    private String id;
    private String fullName;
    private String emailAddress;
    private String password;
    private String phoneNumber;
    private String role;
    private String nameOnBoard;
    private String latitude;
    private String longitude;
    private String profileImage;
    private String uUid;
    private String fcmToken;
    private String rating;
    private List<Service> services = new ArrayList<>();
    private List<Bundle> bundles = new ArrayList<>();
    private List<Offer> offers = new ArrayList<>();
    private List<String> reviewGivers = new ArrayList<>();
    private List<String> likeGivers = new ArrayList<>();
    private List<Activities> activities = new ArrayList<>();
    private List<Notifications> notifications = new ArrayList<>();
    private List<LoginHistory> loginHistory = new ArrayList<>();

    public UserModel() {
    }

    public UserModel(String fullName, String emailAddress, String phoneNumber, String password, String role) {
        this.fullName = fullName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.role = role;
    }

    public UserModel(String fullName, String emailAddress, String password, String phoneNumber, String role, String nameOnBoard, String latitude, String longitude) {
        this.fullName = fullName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.nameOnBoard = nameOnBoard;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public List<Bundle> getBundles() {
        return bundles;
    }

    public void setBundles(List<Bundle> bundles) {
        this.bundles = bundles;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<LoginHistory> getLoginHistory() {
        return loginHistory;
    }

    public void setLoginHistory(List<LoginHistory> loginHistory) {
        this.loginHistory = loginHistory;
    }

    public List<Notifications> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notifications> notifications) {
        this.notifications = notifications;
    }

    public List<Activities> getActivities() {
        return activities;
    }

    public void setActivities(List<Activities> activities) {
        this.activities = activities;
    }

    public List<String> getLikeGivers() {
        return likeGivers;
    }

    public void setLikeGivers(List<String> likeGivers) {
        this.likeGivers = likeGivers;
    }

    public List<String> getReviewGivers() {
        return reviewGivers;
    }

    public void setReviewGivers(List<String> reviewGivers) {
        this.reviewGivers = reviewGivers;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getuUid() {
        return uUid;
    }

    public void setuUid(String uUid) {
        this.uUid = uUid;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNameOnBoard() {
        return nameOnBoard;
    }

    public void setNameOnBoard(String nameOnBoard) {
        this.nameOnBoard = nameOnBoard;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public static Comparator<UserModel> name = (s1, s2) -> {

        String UserName1 = s1.getNameOnBoard().toUpperCase();
        String UserName2 = s2.getNameOnBoard().toUpperCase();

//            ascending order
        return UserName1.compareTo(UserName2);

////            descending order
//            return StudentName2.compareTo(StudentName1);
    };

    public static Comparator<UserModel> rate = (s1, s2) -> {

        Double rating1 = 0.0;
        Double rating2 = 0.0;

        if (s1.getRating() != null) {
            rating1 = Double.parseDouble(s1.getRating()) / s1.getReviewGivers().size();
        }
        if (s2.getRating() != null)
            rating2 = Double.parseDouble(s2.getRating()) / s2.getReviewGivers().size();

//            ascending order
        return rating2.compareTo(rating1);

////            descending order
//            return StudentName2.compareTo(StudentName1);
    };

}
