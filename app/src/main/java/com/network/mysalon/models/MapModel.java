package com.network.mysalon.models;

import com.google.android.gms.maps.model.LatLng;

public class MapModel {

    private String title, image;
    private LatLng latLng;

    public MapModel(String title, String image, LatLng latLng) {
        this.title = title;
        this.image = image;
        this.latLng = latLng;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
