package com.network.mysalon.models;

import java.io.Serializable;

public class CartItem implements Serializable {

    String title, type;
    double price;

    public CartItem() {
    }

    public CartItem(String title, String type, double price) {
        this.title = title;
        this.type = type;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
