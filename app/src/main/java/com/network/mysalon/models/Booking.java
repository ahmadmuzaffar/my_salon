package com.network.mysalon.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Booking implements Serializable {

    public enum STATUS {
        ALL,
        ACTIVE,
        REQUEST,
        COMPLETED,
        CANCELLED
    }

    String id;
    UserModel user;
    UserModel salon;
    STATUS status;
    String scheduledDateTime;
    String dateTime;
    List<Service> services = new ArrayList<>();
    List<Bundle> bundles = new ArrayList<>();
    double totalPayment;

    public Booking(String id, UserModel user, UserModel salon, STATUS status, String scheduledDateTime, String dateTime, List<Service> services, List<Bundle> bundles, double totalPayment) {
        this.id = id;
        this.user = user;
        this.salon = salon;
        this.status = status;
        this.scheduledDateTime = scheduledDateTime;
        this.dateTime = dateTime;
        this.services = services;
        this.bundles = bundles;
        this.totalPayment = totalPayment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Bundle> getBundles() {
        return bundles;
    }

    public void setBundles(List<Bundle> bundles) {
        this.bundles = bundles;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public UserModel getSalon() {
        return salon;
    }

    public void setSalon(UserModel salon) {
        this.salon = salon;
    }

    public Booking() {
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public String getScheduledDateTime() {
        return scheduledDateTime;
    }

    public void setScheduledDateTime(String scheduledDateTime) {
        this.scheduledDateTime = scheduledDateTime;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(double totalPayment) {
        this.totalPayment = totalPayment;
    }
}
