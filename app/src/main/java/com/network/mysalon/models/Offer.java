package com.network.mysalon.models;

import java.io.Serializable;
import java.util.List;

public class Offer implements Serializable {

    private String id;
    private List<String> serviceIds;
    private List<String> bundleIds;
    private String description;
    private String name;
    private String image;
    private String type;
    private double oldPrice;
    private double newPrice;

    public Offer() {
    }

    public Offer(String id, List<String> serviceIds, List<String> bundleIds, String description, String name, String image, String type, double oldPrice, double newPrice) {
        this.id = id;
        this.serviceIds = serviceIds;
        this.bundleIds = bundleIds;
        this.description = description;
        this.name = name;
        this.image = image;
        this.type = type;
        this.oldPrice = oldPrice;
        this.newPrice = newPrice;
    }

    public List<String> getBundleIds() {
        return bundleIds;
    }

    public void setBundleIds(List<String> bundleIds) {
        this.bundleIds = bundleIds;
    }

    public double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(double oldPrice) {
        this.oldPrice = oldPrice;
    }

    public double getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(double newPrice) {
        this.newPrice = newPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(List<String> serviceIds) {
        this.serviceIds = serviceIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
