package com.network.mysalon.models;

import java.io.Serializable;
import java.util.List;

public class Bundle implements Serializable {

    private String id;
    private List<SelectedService> serviceIds;
    private String description;
    private String name;
    private String image;
    private double price;

    public Bundle() {
    }

    public Bundle(String id, List<SelectedService> serviceIds, String description, String name, String image, double price) {
        this.id = id;
        this.serviceIds = serviceIds;
        this.description = description;
        this.name = name;
        this.image = image;
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SelectedService> getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(List<SelectedService> serviceIds) {
        this.serviceIds = serviceIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
