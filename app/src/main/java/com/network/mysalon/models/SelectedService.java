package com.network.mysalon.models;

import java.io.Serializable;

public class SelectedService implements Serializable {

    private String id;
    private String title;
    private double price;

    public SelectedService() {
    }

    public SelectedService(String title, double price) {
        this.title = title;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
