package com.network.mysalon.models;

import java.io.Serializable;
import java.util.Comparator;

public class Activities implements Serializable {

    private String title, description, dateTime;

    public Activities() {
    }

    public Activities(String title, String description, String dateTime) {
        this.title = title;
        this.description = description;
        this.dateTime = dateTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public static Comparator<Activities> sortDesc = (s1, s2) -> {
        return s2.getDateTime().toUpperCase().compareTo(s1.getDateTime().toUpperCase());
    };
}
