package com.network.mysalon.models;

public class SalonItem {

    String salonId, salonName, salonContact;
    int salonLogo;

    public SalonItem(String salonId, String salonName, String salonContact, int salonLogo) {
        this.salonId = salonId;
        this.salonName = salonName;
        this.salonContact = salonContact;
        this.salonLogo = salonLogo;
    }

    public String getSalonId() {
        return salonId;
    }

    public void setSalonId(String salonId) {
        this.salonId = salonId;
    }

    public String getSalonName() {
        return salonName;
    }

    public void setSalonName(String salonName) {
        this.salonName = salonName;
    }

    public String getSalonContact() {
        return salonContact;
    }

    public void setSalonContact(String salonContact) {
        this.salonContact = salonContact;
    }

    public int getSalonLogo() {
        return salonLogo;
    }

    public void setSalonLogo(int salonLogo) {
        this.salonLogo = salonLogo;
    }
}
