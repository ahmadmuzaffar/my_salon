package com.network.mysalon.models;

import java.io.Serializable;
import java.util.Comparator;

public class Notifications implements Serializable {

    private String notificationId,
            notificationTitle,
            notificationBody,
            notificationBy,
            notificationByImage,
            notificationDateTime;
    private boolean isNotificationRead;

    public Notifications() {
    }

    public Notifications(String notificationId, String notificationTitle, String notificationBody, String notificationBy, String notificationByImage, String notificationDateTime, boolean isNotificationRead) {
        this.notificationId = notificationId;
        this.notificationTitle = notificationTitle;
        this.notificationBody = notificationBody;
        this.notificationBy = notificationBy;
        this.notificationByImage = notificationByImage;
        this.notificationDateTime = notificationDateTime;
        this.isNotificationRead = isNotificationRead;
    }

    public String getNotificationByImage() {
        return notificationByImage;
    }

    public void setNotificationByImage(String notificationByImage) {
        this.notificationByImage = notificationByImage;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationBody() {
        return notificationBody;
    }

    public void setNotificationBody(String notificationBody) {
        this.notificationBody = notificationBody;
    }

    public String getNotificationBy() {
        return notificationBy;
    }

    public void setNotificationBy(String notificationBy) {
        this.notificationBy = notificationBy;
    }

    public String getNotificationDateTime() {
        return notificationDateTime;
    }

    public void setNotificationDateTime(String notificationDateTime) {
        this.notificationDateTime = notificationDateTime;
    }

    public boolean isNotificationRead() {
        return isNotificationRead;
    }

    public void setNotificationRead(boolean notificationRead) {
        isNotificationRead = notificationRead;
    }

    public static Comparator<Notifications> sortDesc = (s1, s2) -> {
        return s2.getNotificationDateTime().toUpperCase().compareTo(s1.getNotificationDateTime().toUpperCase());
    };
}
