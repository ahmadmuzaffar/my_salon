package com.network.mysalon.models;

import java.io.Serializable;

public class LoginHistory implements Serializable {

    private String loginDateTime, logoutDateTime;

    public LoginHistory() {
    }

    public LoginHistory(String loginDateTime, String logoutDateTime) {
        this.loginDateTime = loginDateTime;
        this.logoutDateTime = logoutDateTime;
    }

    public String getLoginDateTime() {
        return loginDateTime;
    }

    public void setLoginDateTime(String loginDateTime) {
        this.loginDateTime = loginDateTime;
    }

    public String getLogoutDateTime() {
        return logoutDateTime;
    }

    public void setLogoutDateTime(String logoutDateTime) {
        this.logoutDateTime = logoutDateTime;
    }

}
