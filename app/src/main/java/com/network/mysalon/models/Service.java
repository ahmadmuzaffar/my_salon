package com.network.mysalon.models;

import java.io.Serializable;

public class Service implements Serializable {

    private String id;
    private String image;
    private String name;
    private String description;
    private String duration;
    private String type;
    private double price;

    public Service() {
    }

    public Service(String id, String name, String description, String duration, String image, String type, double price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.duration = duration;
        this.image = image;
        this.type = type;
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
