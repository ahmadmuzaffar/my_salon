package com.network.mysalon.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.network.mysalon.models.Booking;
import com.network.mysalon.models.UserModel;

public class UserPreferences {

    private final String PREF_FILE_APP = "ApplicationDetails";

    private final String PREF_LOGGED_IN = "loggedIn";

    private Context mContext = null;

    public static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public UserPreferences(Context context) {
        this.mContext = context;
    }

    public void clearPreferences() {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
        editor.clear().apply();
    }

    public boolean setUser(UserModel value) {
        try {
            SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
            editor.putString("user", new Gson().toJson(value));
            return editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public UserModel getUser() {
        try {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
            String value = sharedPreferences.getString("user", "");
            return new Gson().fromJson(value, UserModel.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean setCartData(Booking value) {
        try {
            SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
            if (value == null)
                editor.putString("booking", null);
            else
                editor.putString("booking", new Gson().toJson(value));
            return editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Booking getCartData() {
        try {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
            String value = sharedPreferences.getString("booking", null);
            return value != null ? new Gson().fromJson(value, Booking.class) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}