package com.network.mysalon.utilities;

import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.network.mysalon.models.Notifications;
import com.network.mysalon.models.UserModel;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import androidx.annotation.NonNull;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FcmNotifier {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static void sendNotification(final String messageBody, final String title, UserModel sender, UserModel receiver) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {

                    if (receiver.getFcmToken() == null) {
                        Date date = new Date();
                        date.setTime(new Date().getTime());
                        Notifications notification = new Notifications(String.valueOf(new Random().nextInt(50000)), title, messageBody, sender.getId(), sender.getProfileImage(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(date), false);
                        FirebaseDatabase.getInstance().getReference().child("users/" + receiver.getRole() + "/" + receiver.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                UserModel user = snapshot.getValue(UserModel.class);
                                user.getNotifications().add(notification);
                                FirebaseDatabase.getInstance().getReference().child("users/" + user.getRole() + "/" + user.getId()).setValue(user);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                        return null;
                    }

                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();
                    JSONObject notificationObj = new JSONObject();
                    JSONObject dataObj = new JSONObject();
                    notificationObj.put("body", messageBody);
                    notificationObj.put("title", title);
                    dataObj.put("senderId", sender.getId());
                    dataObj.put("senderImage", sender.getProfileImage());
                    dataObj.put("sentTime", new Date().getTime());
                    json.put("notification", notificationObj);
                    json.put("data", dataObj);
                    json.put("messageId", String.valueOf(new Random().nextInt(50000)));
                    json.put("to", receiver.getFcmToken());

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=AAAAHcH5_2Y:APA91bEL5F3m27URCbGah8xf5lv3Oc6I9T8ODBRRYNQSrjw_uZ1WANP64RiUKeV6ITOCdDjjsMf-2RhX_5mehHdP8P05W1eJtmeKXqS6VxBwaqqhsfrt9LbkafgEW97z5_jUC5BHQxRJ")
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                    Log.i("Push Notification", finalResponse);
                    Date date = new Date();
                    date.setTime(json.getJSONObject("data").getLong("sentTime"));
                    Notifications notification = new Notifications(json.getString("messageId"), title, messageBody, sender.getId(), sender.getProfileImage(), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(date), false);
                    FirebaseDatabase.getInstance().getReference().child("users/" + receiver.getRole() + "/" + receiver.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            UserModel user = snapshot.getValue(UserModel.class);
                            user.getNotifications().add(notification);
                            FirebaseDatabase.getInstance().getReference().child("users/" + user.getRole() + "/" + user.getId()).setValue(user);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }
}
