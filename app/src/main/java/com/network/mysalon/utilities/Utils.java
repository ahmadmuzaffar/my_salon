package com.network.mysalon.utilities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.network.mysalon.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.core.content.FileProvider;

public class Utils {

    public static boolean isValidEmail(String emailAddress) {

        try {
            Pattern pattern = Pattern.compile("^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
            Matcher matcher = pattern.matcher(emailAddress);
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    public static Uri getUriFromBitmap(Bitmap bitmap, Context context) {

        File file = new File(context.getExternalCacheDir(), System.currentTimeMillis() + ".jpeg");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] bytearray = byteArrayOutputStream.toByteArray();
        try {
            fileOutputStream.write(bytearray);
            fileOutputStream.flush();
            fileOutputStream.close();
            byteArrayOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return FileProvider.getUriForFile(context, "com.network.mysalon.fileprovider", file);
    }

    public static void setScaledPic(ImageView imageView, String currentPhotoPath) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(currentPhotoPath, bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH));

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
        imageView.setImageBitmap(bitmap);
    }

    public static float dpToPx(float dp) {
        return (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private void galleryAddPic(Context context, Uri uri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        File f = new File(currentPhotoPath);
//        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(uri);
        context.sendBroadcast(mediaScanIntent);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Context context, Uri uri) {
        String path = "";
        if (context.getContentResolver() != null) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    public static void openCalender(final Context context, final TextView editText, String minDate, String errorMessage, String format) {
        try {
            final int mYear, mMonth, mDay;

            final Calendar c = Calendar.getInstance();
            final Calendar calendarMinDate = Calendar.getInstance();
            if (minDate != null) {
                final SimpleDateFormat sdf = new SimpleDateFormat(format);
                Date mDate = sdf.parse(minDate);
                calendarMinDate.setTime(mDate);
            }

            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.DatePickerTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            if (minDate != null) {
                                if (year < calendarMinDate.get(Calendar.YEAR) ||
                                        (year == calendarMinDate.get(Calendar.YEAR) && monthOfYear < calendarMinDate.get(Calendar.MONTH))
                                        || (year == calendarMinDate.get(Calendar.YEAR) && monthOfYear == calendarMinDate.get(Calendar.MONTH) && dayOfMonth < calendarMinDate.get(Calendar.DAY_OF_MONTH))) {
                                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                } else {
                                    c.set(year, monthOfYear, dayOfMonth);
                                    editText.setText(convertDateToString(c, format));
                                }
                            } else {
                                c.set(year, monthOfYear, dayOfMonth);
                                editText.setText(convertDateToString(c, format));
                            }
                        }
                    }, mYear, mMonth, mDay);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.MINUTE, calendar.getMaximum(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, calendar.getMaximum(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));

            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String convertDateToString(Calendar calendar, String format) {
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        final String strDate = sdf.format(calendar.getTime());
        return strDate;
    }

}
