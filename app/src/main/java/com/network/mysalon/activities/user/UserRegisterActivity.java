package com.network.mysalon.activities.user;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.network.mysalon.activities.PreviewActivity;
import com.network.mysalon.R;
import com.network.mysalon.models.LoginHistory;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.utilities.Utils;
import com.network.mysalon.activities.salon.VendorDashboardActivity;
import com.network.mysalon.databinding.ActivityUserRegisterBinding;
import com.network.mysalon.models.UserModel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserRegisterActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_CODE = 1001;
    private static final int PIC_CROP_CODE = 1002;
    private static final int CAMERA_PIC_REQUEST_CODE = 1003;
    private Uri photoURI;
    ActivityUserRegisterBinding mBinding;
    private FirebaseAuth auth;
    private File file;
    private String imagePath;
    private Dialog d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_register);

        mBinding.btnSignUp.setOnClickListener(v -> {
            if (TextUtils.isEmpty(mBinding.fullNameField.getText()) ||
                    TextUtils.isEmpty(mBinding.emailField.getText()) ||
                    TextUtils.isEmpty(mBinding.phoneField.getText()) ||
                    TextUtils.isEmpty(mBinding.passwordField.getText()) ||
                    TextUtils.isEmpty(mBinding.confirmPasswordField.getText())) {
                Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
            } else {
                if (Utils.isValidEmail(mBinding.emailField.getText().toString().trim())) {
                    if (mBinding.passwordField.length() > 5 && mBinding.passwordField.length() < 15) {
                        if (mBinding.confirmPasswordField.length() > 5 && mBinding.confirmPasswordField.length() <= 15) {
                            if (mBinding.passwordField.getText().toString().equals(mBinding.confirmPasswordField.getText().toString())) {
                                d = new Dialog(this);
                                d.setContentView(R.layout.dialog_loader);
                                d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                d.setCancelable(false);
                                d.setCanceledOnTouchOutside(false);

                                d.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface dialogInterface) {
                                        registerUserToFirebase();
                                    }
                                });

                                d.show();
                            } else
                                Toast.makeText(this, "Passwords did not match", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(this, "Password should be 6-15 characters long", Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(this, "Password should be 6-15 characters long", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(this, "Invalid email", Toast.LENGTH_SHORT).show();
            }
        });

        mBinding.btnCapture.setOnClickListener(v -> {
            showPictureDialog();
        });

        mBinding.profilePicture.setOnClickListener(v -> {
            if (photoURI != null)
                startActivity(new Intent(this, PreviewActivity.class).putExtra("imageUri", photoURI.toString()));
        });
    }

    private void registerUserToFirebase() {

        final UserModel[] user = {new UserModel(mBinding.fullNameField.getText().toString(),
                mBinding.emailField.getText().toString(),
                mBinding.phoneField.getText().toString(),
                mBinding.passwordField.getText().toString(),
                "user")};

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();
        DatabaseReference usersRef = ref.child("users");
        DatabaseReference vendorRef = usersRef.child(user[0].getRole());
        DatabaseReference newVendorRef = vendorRef.push();
        user[0].setId(newVendorRef.getKey());
        newVendorRef.setValue(user[0]);

        if (photoURI != null) {
            Uri file = Uri.fromFile(new File(photoURI.getPath()));
            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            StorageReference imageRef = storageRef.child("Images/" + user[0].getId() + "/ProfileImages/" + photoURI.getLastPathSegment());
            UploadTask uploadTask = imageRef.putFile(file);

            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    d.dismiss();
                    return null;
                }

                return imageRef.getDownloadUrl();
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        user[0].setProfileImage(downloadUri.toString());
                    }
                }
            });
        }

        auth = FirebaseAuth.getInstance();
        auth.createUserWithEmailAndPassword(user[0].getEmailAddress(), user[0].getPassword())
                .addOnCompleteListener(UserRegisterActivity.this, task -> {
                    if (!task.isSuccessful()) {
                        Toast.makeText(UserRegisterActivity.this, "Authentication failed." + task.getException(),
                                Toast.LENGTH_SHORT).show();
                        d.dismiss();
                    } else {

                        FirebaseInstanceId.getInstance().getInstanceId()
                                .addOnCompleteListener(task1 -> {
                                    if (!task.isSuccessful()) {
                                        d.dismiss();
                                        Log.w("TAG", "Fetching FCM registration token failed", task.getException());
                                        return;
                                    }

                                    // Get new FCM registration token
                                    String token = task1.getResult().getToken();
                                    Log.d("TAG", "generateToken: " + token);

                                    FirebaseDatabase.getInstance().getReference().child("users/" + user[0].getRole() + "/" + user[0].getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            user[0] = snapshot.getValue(UserModel.class);
                                            user[0].setFcmToken(token);

                                            String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date());
                                            user[0].getLoginHistory().add(new LoginHistory(date, ""));

                                            new UserPreferences(UserRegisterActivity.this).setUser(user[0]);
                                            FirebaseDatabase.getInstance().getReference().child("users/" + user[0].getRole() + "/" + user[0].getId()).setValue(user[0]);

                                            startActivity(new Intent(UserRegisterActivity.this, UserDashboardActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                            d.dismiss();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {
                                        }
                                    });
                                });
                    }
                });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_IMAGE_CODE) {
                photoURI = data.getData();

                imagePath = photoURI.getPath();
                Cursor cursor = null;
                try {
                    String[] proj = {MediaStore.Images.Media.DATA};
                    cursor = getContentResolver().query(photoURI, proj, null, null, null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    imagePath = cursor.getString(column_index);
                } catch (Exception e) {
                    imagePath = photoURI.getPath();
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
                mBinding.profilePicture.setImageURI(photoURI);
            } else if (requestCode == CAMERA_PIC_REQUEST_CODE) {
                photoURI = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".fileprovider", file);
                mBinding.profilePicture.setImageURI(photoURI);
            } else if (requestCode == PIC_CROP_CODE) {
                if (data != null) {
//                    Bundle extras = data.getExtras();
//                    Bitmap selectedBitmap = extras.getParcelable("data");
//                    photoURI = Utils.getUriFromBitmap(selectedBitmap, this);
                    photoURI = Uri.parse(data.getStringExtra("data"));
//                    Utils.setScaledPic(mBinding.profilePicture, new File(String.valueOf(Utils.getUriFromBitmap(selectedBitmap, this))).getPath());
                    mBinding.profilePicture.setImageURI(photoURI);
                }
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            }
        } else if (requestCode == 2) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera();
            }
        }
    }

    private void performCrop(Uri picUri) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", true);
            cropIntent.putExtra("scale", true);
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 400);
            cropIntent.putExtra("outputY", 400);
            cropIntent.putExtra("return-data", true);
            cropIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(cropIntent, PIC_CROP_CODE);
        } catch (ActivityNotFoundException anfe) {
            anfe.printStackTrace();
            String errorMessage = "Cropping Error";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @SuppressLint("IntentReset")
    public void showPictureDialog() {

        final Dialog d = new Dialog(UserRegisterActivity.this);
        d.setContentView(R.layout.dialog_capture);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(d.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        d.show();
        d.getWindow().setAttributes(layoutParams);

        TextView btnCamera, btnGallery;
        btnCamera = d.findViewById(R.id.btnCamera);
        btnGallery = d.findViewById(R.id.btnGallery);

        btnCamera.setOnClickListener(v -> {
            if (checkCameraPermission()) {
                openCamera();
            } else {
                ActivityCompat.requestPermissions(UserRegisterActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        2);
            }
            d.dismiss();
        });

        btnGallery.setOnClickListener(v -> {
            if (checkReadPermission() && checkWritePermission()) {
                openGallery();
            } else {
                ActivityCompat.requestPermissions(UserRegisterActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
            d.dismiss();
        });
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, PICK_IMAGE_CODE);
    }

    private void openCamera() {
        Intent m_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        Uri uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".fileprovider", file);
        m_intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(m_intent, CAMERA_PIC_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        String permission = Manifest.permission.CAMERA;
        int res = checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkReadPermission() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        int res = checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkWritePermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void galleryAddPic(String currentPhotoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

}