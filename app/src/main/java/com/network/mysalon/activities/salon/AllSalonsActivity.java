package com.network.mysalon.activities.salon;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.network.mysalon.R;
import com.network.mysalon.adapters.AllSalonsAdapter;
import com.network.mysalon.databinding.ActivityAllSalonsBinding;
import com.network.mysalon.fragments.salon.SalonDetailFragment;
import com.network.mysalon.models.Activities;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.FcmNotifier;
import com.network.mysalon.utilities.UserPreferences;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class AllSalonsActivity extends AppCompatActivity implements AllSalonsAdapter.SalonActions {

    private ActivityAllSalonsBinding mBinding;
    private ArrayList<UserModel> salonItems = new ArrayList<>();
    private ArrayList<UserModel> tempList = new ArrayList<>();
    private AllSalonsAdapter allSalonsAdapter = null;
    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_all_salons);

        userModel = new UserPreferences(this).getUser();

        mBinding.back.setOnClickListener(v -> {
            onBackPressed();
        });

        mBinding.searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (tempList.size() != 0) {
                    if (editable.length() == 0) {
                        salonItems.clear();
                        salonItems.addAll(tempList);
                    } else {

                        String text = mBinding.searchView.getText().toString().toLowerCase();
                        salonItems.clear();
                        for (UserModel salon : tempList) {
                            if (salon.getNameOnBoard().toLowerCase().contains(text)) {
                                salonItems.add(salon);
                            }
                        }

                    }
                    allSalonsAdapter.notifyDataSetChanged();
                }
            }
        });

        mBinding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mBinding.searchView.setText("");
                setAdapter();
                mBinding.swipe.setRefreshing(false);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        setAdapter();
    }

    private void setAdapter() {

        FirebaseDatabase.getInstance().getReference().child("users/vendor").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                salonItems.clear();
                for (DataSnapshot userSnapshot : snapshot.getChildren()) {
                    UserModel userModel = userSnapshot.getValue(UserModel.class);
                    salonItems.add(userModel);
                }
                if (salonItems.size() == 0) {
                    mBinding.allSalons.setVisibility(View.GONE);
                    mBinding.noSalon.setVisibility(View.VISIBLE);
                } else {

                    mBinding.allSalons.setVisibility(View.VISIBLE);
                    mBinding.noSalon.setVisibility(View.GONE);

                    tempList.addAll(salonItems);
                    Collections.sort(salonItems, UserModel.name);
                    allSalonsAdapter = new AllSalonsAdapter(salonItems, AllSalonsActivity.this);
                    allSalonsAdapter.setSalonActions(AllSalonsActivity.this);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(AllSalonsActivity.this);
                    mBinding.allSalons.setLayoutManager(layoutManager);
                    mBinding.allSalons.setItemAnimator(new DefaultItemAnimator());
                    mBinding.allSalons.setAdapter(allSalonsAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    private void showReviewDialog(UserModel obj) {
        final Dialog d = new Dialog(this);
        d.setContentView(R.layout.dialog_rating);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView btnGiveReview, btnCancel;
        EditText reviewValue;
        RatingBar ratingBar;

        btnGiveReview = d.findViewById(R.id.btnGiveReview);
        btnCancel = d.findViewById(R.id.btnCancel);
        reviewValue = d.findViewById(R.id.reviewValue);
        ratingBar = d.findViewById(R.id.ratingBar);

        ratingBar.setOnTouchListener(new View.OnTouchListener() {
            private float downXValue;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    downXValue = event.getX();
                    return false;
                }

                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    // When true is returned, view will not handle this event.
                    return true;
                }

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    float currentX = event.getX();
                    float difference = 0;
                    // Swipe on left side
                    if (currentX < downXValue)
                        difference = downXValue - currentX;
                        // Swipe on right side
                    else if (currentX > downXValue)
                        difference = currentX - downXValue;

                    if (difference < 10)
                        return false;

                    return true;
                }
                return false;
            }
        });

        reviewValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    ratingBar.setRating(Float.parseFloat(s.toString()));
                }
            }
        });

        btnGiveReview.setOnClickListener(v -> {
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            double rating = 0.0;
            if (obj.getRating() != null)
                rating = Double.parseDouble(obj.getRating());
            rating += ratingBar.getRating();

            obj.getReviewGivers().add(userModel.getId());
            obj.setRating(String.valueOf(rating));

            userModel.getActivities().add(new Activities("Rating", "You rated " + obj.getNameOnBoard() + "'s profile " + ratingBar.getRating() + "/5", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
            database.getReference().child("users/" + userModel.getRole() + "/" + userModel.getId()).setValue(userModel);
            database.getReference().child("users/" + obj.getRole() + "/" + obj.getId()).setValue(obj);
            allSalonsAdapter.notifyDataSetChanged();
            d.dismiss();

        });

        btnCancel.setOnClickListener(v -> {
            d.dismiss();
        });

        d.show();
    }

    @Override
    public void performRate(UserModel obj) {
        if (obj.getReviewGivers().size() > 0) {
            for (String reviewGiver : obj.getReviewGivers())
                if (reviewGiver.contains(userModel.getId())) {
                    Toast.makeText(this, "Review already given", Toast.LENGTH_SHORT).show();
                    return;
                }
        }

        showReviewDialog(obj);
    }

    @Override
    public void performLike(UserModel obj) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();

        boolean isFound = false;
        int index = salonItems.indexOf(obj);
        if (obj.getLikeGivers().size() > 0) {
            for (String likeGiver : obj.getLikeGivers()) {
                if (likeGiver.contains(userModel.getId())) {
                    obj.getLikeGivers().remove(likeGiver);
                    isFound = true;
                    break;
                }
            }

            if (!isFound) {

                userModel.getActivities().add(new Activities("Like", "You liked " + obj.getNameOnBoard() + "'s profile", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));

                FcmNotifier.sendNotification(userModel.getFullName() + " liked your profile", "Like Notification", userModel, obj);
                obj.getLikeGivers().add(userModel.getId());
            } else {
                userModel.getActivities().add(new Activities("Like", "You dis-liked " + obj.getNameOnBoard() + "'s profile", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
                FcmNotifier.sendNotification(userModel.getFullName() + " disliked your profile", "Like Notification", userModel, obj);
                obj.getLikeGivers().remove(userModel.getId());
            }
        } else {
            userModel.getActivities().add(new Activities("Like", "You liked " + obj.getNameOnBoard() + "'s profile", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));

            FcmNotifier.sendNotification(userModel.getFullName() + " liked your profile", "Like Notification", userModel, obj);
            obj.getLikeGivers().add(userModel.getId());
        }

        database.getReference().child("users/" + userModel.getRole() + "/" + userModel.getId()).setValue(userModel);
        database.getReference().child("users/" + obj.getRole() + "/" + obj.getId()).setValue(obj);
        salonItems.set(index, obj);
        allSalonsAdapter.notifyItemChanged(index);
        new UserPreferences(this).setUser(userModel);

    }

    @Override
    public void openSalonDetail(UserModel obj) {

        Fragment mFragment = new SalonDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", obj);
        mFragment.setArguments(bundle);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragmentContainer, mFragment, "menu_fragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }
}