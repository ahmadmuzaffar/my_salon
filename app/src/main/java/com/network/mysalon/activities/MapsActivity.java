package com.network.mysalon.activities;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.network.mysalon.R;
import com.network.mysalon.fragments.salon.SalonDetailFragment;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.utilities.Utils;
import com.network.mysalon.databinding.ActivityMapsBinding;
import com.network.mysalon.models.UserModel;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private static final String TAG = "Maps Activity";
    GoogleMap map;
    ActivityMapsBinding mBinding;
    List<UserModel> markers = new ArrayList<>();
    List<UserModel> searchedMarkers = new ArrayList<>();
    private UserModel self;
    private SupportMapFragment mapFragment;
    private Marker myMarker;
    private String latitude;
    private String longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_maps);
        self = new UserPreferences(this).getUser();
        latitude = getIntent().getStringExtra("lat");
        longitude = getIntent().getStringExtra("long");
        self.setLatitude(latitude);
        self.setLongitude(longitude);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        loadVendorsLocation();

        mBinding.searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    searchedMarkers.clear();
                    searchedMarkers.addAll(markers);
                    if (map != null) {
                        map.clear();
                        createMarkers(map);
                    }
                }
            }
        });

        mBinding.searchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });
    }

    private void performSearch() {
        searchedMarkers.clear();
        for (UserModel mapModel : markers) {
            if (mapModel.getId().equals(self.getId())) {
                if (mapModel.getFullName().toLowerCase().contains(mBinding.searchView.getText().toString().toLowerCase())) {
                    searchedMarkers.add(mapModel);
                }
            } else if (mapModel.getNameOnBoard().toLowerCase().contains(mBinding.searchView.getText().toString().toLowerCase())) {
                searchedMarkers.add(mapModel);
            }
        }
        if (map != null && searchedMarkers.size() > 0) {
            map.clear();
            createMarkers(map);
        }
    }

    private void loadVendorsLocation() {

        FirebaseDatabase.getInstance().getReference().child("users/vendor").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot userSnapshot : snapshot.getChildren()) {
                    UserModel userModel = userSnapshot.getValue(UserModel.class);
                    markers.add(userModel);
                }
                markers.add(self);
                searchedMarkers.clear();
                searchedMarkers.addAll(markers);
                mapFragment.getMapAsync(MapsActivity.this::onMapReady);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
//        Toast.makeText(this, "Showing you all vendors...", Toast.LENGTH_SHORT).show();
        map = googleMap;
        createMarkers(map);
    }

    private void createMarkers(GoogleMap map) {

        map.setOnMarkerClickListener(this);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (UserModel mapModel : searchedMarkers) {
            builder.include(new LatLng(Double.parseDouble(mapModel.getLatitude()), Double.parseDouble(mapModel.getLongitude())));
            MarkerOptions options = new MarkerOptions();
            options.position(new LatLng(Double.parseDouble(mapModel.getLatitude()), Double.parseDouble(mapModel.getLongitude())));
            options.anchor(0.5f, 0.5f);
//                    .title(mapModel.getTitle())
//                    .snippet(snippet)
//                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.barber)));
            options.icon(BitmapDescriptorFactory.fromBitmap(createStoreMarker(mapModel)));
            myMarker = map.addMarker(options);
//            myMarker.showInfoWindow();
//                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapModel.getLatLng(), 15));
//                            googleMap.animateCamera(CameraUpdateFactory.zoomIn());
//                            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        }
        LatLngBounds bounds = builder.build();
        int padding = 100;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//                        googleMap.moveCamera(cu);
        map.animateCamera(cu);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        for (UserModel checkingMarker : searchedMarkers) {
            if (!checkingMarker.getId().equals(self.getId())) {
                if (marker.getPosition().equals(new LatLng(Double.parseDouble(checkingMarker.getLatitude()), Double.parseDouble(checkingMarker.getLongitude())))) {
                    showCustomDialog(checkingMarker.getId());
                    return true;
                }
            }
        }

        return false;
    }

    private Bitmap createStoreMarker(UserModel model) {
        View markerLayout = getLayoutInflater().inflate(R.layout.custom_map_marker, null);

        TextView markerRating = markerLayout.findViewById(R.id.marker_text);
        ImageView bitmapp = markerLayout.findViewById(R.id.marker_image);
        if (model.getId().equals(self.getId())) {
            markerRating.setText(model.getFullName());
            bitmapp.setImageDrawable(ContextCompat.getDrawable(MapsActivity.this, R.drawable.pin));
        } else
            markerRating.setText(model.getNameOnBoard());

        markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        markerLayout.layout(0, 0, markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight());

        final Bitmap bitmap = Bitmap.createBitmap(markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        markerLayout.draw(canvas);
        return bitmap;
    }

    private void showCustomDialog(String salonId) {

        final UserModel[] salon = {null};
        final Dialog d = new Dialog(MapsActivity.this);
        UserModel user = new UserPreferences(MapsActivity.this).getUser();
        d.setContentView(R.layout.dialog_mrker_detail);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(true);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lWindowParams.horizontalMargin = Utils.dpToPx(20);
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        ImageView imageView = d.findViewById(R.id.image);
        TextView title = d.findViewById(R.id.title);
        TextView address = d.findViewById(R.id.address);
        TextView rating = d.findViewById(R.id.rating);
        RatingBar ratingBar = d.findViewById(R.id.ratingBar);
        ImageView btnLike = d.findViewById(R.id.btnLike);
        TextView likes = d.findViewById(R.id.likes);

        CardView rootContainer = d.findViewById(R.id.rootContainer);

        FirebaseDatabase.getInstance().getReference().child("users/vendor/" + salonId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                salon[0] = snapshot.getValue(UserModel.class);

                if (salon[0].getProfileImage() != null)
                    Glide.with(MapsActivity.this)
                            .load(salon[0].getProfileImage())
                            .placeholder(R.drawable.logo)
                            .into(imageView);

                title.setText(salon[0].getNameOnBoard());

                Geocoder geocoder = new Geocoder(MapsActivity.this, Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(salon[0].getLatitude()), Double.parseDouble(salon[0].getLongitude()), 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                address.setText(addresses.get(0).getAddressLine(0));
                float rate = Float.parseFloat(salon[0].getRating()) / salon[0].getReviewGivers().size();
                ratingBar.setRating(rate);
                rating.setText(String.valueOf(rate));
                if (salon[0].getLikeGivers().contains(user.getId())) {
                    btnLike.setImageDrawable(ContextCompat.getDrawable(MapsActivity.this, R.drawable.ic_like_filled));
                } else
                    btnLike.setImageDrawable(ContextCompat.getDrawable(MapsActivity.this, R.drawable.ic_like_outline));
                likes.setText("(" + salon[0].getLikeGivers().size() + ")");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

        rootContainer.setOnClickListener(v -> {
            Fragment mFragment = new SalonDetailFragment();

            startActivity(new Intent(MapsActivity.this, MenuActivity.class)
                    .putExtra("fragment", (Serializable) mFragment)
                    .putExtra("user", salon[0]));
            d.dismiss();
        });

    }
}