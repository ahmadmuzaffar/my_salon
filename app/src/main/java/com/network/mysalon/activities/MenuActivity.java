package com.network.mysalon.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.network.mysalon.R;
import com.network.mysalon.fragments.LoginHistoryFragment;
import com.network.mysalon.fragments.salon.AddServiceFragment;
import com.network.mysalon.databinding.ActivityMenuBinding;
import com.network.mysalon.models.Booking;
import com.network.mysalon.models.UserModel;

public class MenuActivity extends AppCompatActivity {

    ActivityMenuBinding mBinding;
    Fragment fragment;
    String serviceType;
    private UserModel user;
    private Booking booking;

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragments = manager.findFragmentById(R.id.fragment);
        if (fragments instanceof AddServiceFragment || fragments instanceof LoginHistoryFragment) {
            manager.popBackStackImmediate();
            Fragment current = manager.findFragmentById(R.id.fragment);
            current.onResume();
        } else
            finish();
    }

    private void loadFragment() {
        Fragment mFragment = fragment;
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (!serviceType.isEmpty()) {
            Bundle bundle = new Bundle();
            bundle.putString("service_type", serviceType);
            fragment.setArguments(bundle);
        } else if (user != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("user", user);
            fragment.setArguments(bundle);
        } else if (booking != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("booking", booking);
            fragment.setArguments(bundle);
        }
        transaction.addToBackStack(null);
        transaction.add(R.id.fragment, mFragment, "menu_fragment");
        transaction.commit();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_menu);
        getSupportFragmentManager().addOnBackStackChangedListener(getListener());
        fragment = (Fragment) getIntent().getSerializableExtra("fragment");
        serviceType = getIntent().hasExtra("service_type") ? getIntent().getStringExtra("service_type") : "";
        user = getIntent().hasExtra("user") ? (UserModel) getIntent().getSerializableExtra("user") : null;
        booking = getIntent().hasExtra("booking") ? (Booking) getIntent().getSerializableExtra("booking") : null;

        loadFragment();
    }

    public void openLoginHistory() {

        Fragment mFragment = new LoginHistoryFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragment, mFragment, "login_history_fragment");
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public void openAddService() {

        Fragment mFragment = new AddServiceFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (!serviceType.isEmpty()) {
            Bundle bundle = new Bundle();
            bundle.putString("service_type", serviceType);
            mFragment.setArguments(bundle);
        }
        transaction.add(R.id.fragment, mFragment, "add_service_fragment");
        transaction.addToBackStack(null);
        transaction.commit();

    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                FragmentManager manager = getSupportFragmentManager();

                if (manager != null) {
                    Fragment currFrag = manager.findFragmentById(R.id.fragment);

                    if (currFrag != null)
                        currFrag.onResume();
                }
            }
        };

        return result;
    }

}