package com.network.mysalon.activities.user;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.network.mysalon.fragments.BookingFragment;
import com.network.mysalon.fragments.NotificationFragment;
import com.network.mysalon.fragments.user.HomeFragment;
import com.network.mysalon.fragments.MenuFragment;
import com.network.mysalon.R;
import com.network.mysalon.models.Notifications;
import com.network.mysalon.models.UserModel;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.databinding.ActivityUserDashboardBinding;

import static androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class UserDashboardActivity extends AppCompatActivity {

    private static final int NUM_PAGES = 4;
    private FragmentPagerAdapter pagerAdapter;
    ActivityUserDashboardBinding mBinding;
    UserModel user;

    @Override
    protected void onResume() {
        if (getSupportFragmentManager().findFragmentById(R.id.fragment) != null)
            getSupportFragmentManager().findFragmentById(R.id.fragment).onResume();

        refreshBadge();

        super.onResume();
    }

    public void refreshBadge() {

        if (user.getNotifications() == null)
            return;
        int count = 0;
        user = new UserPreferences(this).getUser();
        for (Notifications item : user.getNotifications()) {
            if (!item.isNotificationRead())
                count += 1;
        }
        if (count > 0) {
            BadgeDrawable badge = mBinding.bottomBar.getOrCreateBadge(R.id.notification_fragment);
            badge.setVisible(true);
            badge.setNumber(count);
            badge.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            badge.setBadgeTextColor(ContextCompat.getColor(this, R.color.black));
        }
    }

    public BadgeDrawable getBadge() {
        return mBinding.bottomBar.getOrCreateBadge(R.id.notification_fragment);
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (currentFragment == null) {
            if (mBinding.viewPager.getCurrentItem() == 0) {
                super.onBackPressed();
            } else {
                mBinding.viewPager.setCurrentItem(0);
            }
        } else {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_dashboard);

        user = new UserPreferences(this).getUser();
        getSupportFragmentManager().addOnBackStackChangedListener(getListener());
        pagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
            @NonNull
            @Override
            public Fragment getItem(int position) {
                Fragment mFragment;
                if (position == 0)
                    mFragment = new HomeFragment();
                else if (position == 1)
                    mFragment = new BookingFragment();
                else if (position == 2)
                    mFragment = new NotificationFragment();
                else
                    mFragment = new MenuFragment();
                return mFragment;
            }

            @Override
            public int getCount() {
                return NUM_PAGES;
            }
        };
//        mBinding.viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mBinding.viewPager.setAdapter(pagerAdapter);

        mBinding.bottomBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.home_fragment) {
                    mBinding.viewPager.setCurrentItem(0);
                    return true;
                } else if (item.getItemId() == R.id.bookings_fragment) {
                    mBinding.viewPager.setCurrentItem(1);
                    return true;
                } else if (item.getItemId() == R.id.notification_fragment) {
                    mBinding.viewPager.setCurrentItem(2);
                    return true;
                } else if (item.getItemId() == R.id.menu_fragment) {
                    mBinding.viewPager.setCurrentItem(3);
                    return true;
                }
                return false;
            }
        });

        mBinding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mBinding.bottomBar.setSelectedItemId(position == 0 ? R.id.home_fragment :
                        position == 1 ? R.id.bookings_fragment :
                                position == 2 ? R.id.notification_fragment :
                                        R.id.menu_fragment);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mBinding.viewPager.setCurrentItem(0);
    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                FragmentManager manager = getSupportFragmentManager();

                if (manager != null) {
                    Fragment currFrag = manager.findFragmentById(R.id.viewPager);

                    if (currFrag != null)
                        currFrag.onResume();
                }
            }
        };

        return result;
    }

}