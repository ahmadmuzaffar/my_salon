package com.network.mysalon.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.network.mysalon.R;
import com.network.mysalon.activities.salon.VendorRegisterActivity;
import com.network.mysalon.activities.user.UserRegisterActivity;
import com.network.mysalon.databinding.ActivityRegisterBinding;

public class RegisterActivity extends AppCompatActivity {

    ActivityRegisterBinding activityRegisterBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);

        clickListeners();

    }

    private void clickListeners() {

        activityRegisterBinding.btnVendor.setOnClickListener(v -> {
            startActivity(new Intent(RegisterActivity.this, VendorRegisterActivity.class));
        });

        activityRegisterBinding.btnConsumer.setOnClickListener(v -> {
            startActivity(new Intent(RegisterActivity.this, UserRegisterActivity.class));
        });

    }
}