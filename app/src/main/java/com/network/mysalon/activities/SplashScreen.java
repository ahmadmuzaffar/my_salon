package com.network.mysalon.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import cn.pedant.SweetAlert.SweetAlertDialog;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.network.mysalon.R;
import com.network.mysalon.utilities.RequestPermissionsHelper;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.activities.salon.VendorDashboardActivity;
import com.network.mysalon.activities.user.UserDashboardActivity;
import com.network.mysalon.dialogs.SweetAlertDialogs;
import com.network.mysalon.models.UserModel;

public class SplashScreen extends AppCompatActivity {

    public static final int PERMISSION_REQUEST_CODE = 0;
    private FirebaseUser user;
    private UserPreferences userPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        userPreferences = new UserPreferences(this);
        generateToken();
        requestPermission();

    }

    private void generateToken() {

        FirebaseApp.initializeApp(this);
        if (userPreferences.getUser() == null)
            return;
        final UserModel[] userModel = {userPreferences.getUser()};

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("TAG", "Fetching FCM registration token failed", task.getException());
                        return;
                    }

                    String token = task.getResult().getToken();
                    Log.d("TAG", "generateToken: " + token);

                    FirebaseDatabase.getInstance().getReference().child("users/" + userModel[0].getRole() + "/" + userModel[0].getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            userModel[0] = snapshot.getValue(UserModel.class);
                            userModel[0].setFcmToken(token);
                            userPreferences.setUser(userModel[0]);
                            FirebaseDatabase.getInstance().getReference().child("users/" + userModel[0].getRole() + "/" + userModel[0].getId()).setValue(userModel[0]);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                });

    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check
            int permission = RequestPermissionsHelper.requestPermission(SplashScreen.this);
            if (permission == RequestPermissionsHelper.requestPermissionCode) {
                ActivityCompat.requestPermissions(SplashScreen.this, RequestPermissionsHelper.persmissionsStringArray, SplashScreen.PERMISSION_REQUEST_CODE);
            } else if (permission == RequestPermissionsHelper.notShouldShowRequestPermissionRationaleCode) {
                showDialogPermissionDenied();
            }
            if (permission == RequestPermissionsHelper.notCheckSelfPermissionCode) {
                user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    UserModel user = userPreferences.getUser();
                    if (user.getRole().equals("user")) {
                        startActivity(new Intent(SplashScreen.this, UserDashboardActivity.class));
                    } else {
                        startActivity(new Intent(SplashScreen.this, VendorDashboardActivity.class));
                    }
                    finish();
                } else
                    processHandler();
            }
        } else {
            user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                UserModel user = userPreferences.getUser();
                if (user.getRole().equals("user")) {
                    startActivity(new Intent(SplashScreen.this, UserDashboardActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashScreen.this, VendorDashboardActivity.class));
                    finish();
                }
            } else
                processHandler();
        }
    }

    private void processHandler() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                finish();
            }
        }, 2000);

    }

    public void showDialogPermissionDenied() {

        SweetAlertDialogs.getInstance().showDialogPermissionDenied(this, new SweetAlertDialogs.OnDialogClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        if (requestCode == SplashScreen.PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                boolean granted = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        granted = false;
                        break;
                    }
                }
                if (granted) {
                    processHandler();
                } else {
                    showDialogPermissionDenied();
                }
            } else {
                showDialogPermissionDenied();
            }
        }
    }
}