package com.network.mysalon.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.network.mysalon.R;
import com.network.mysalon.models.LoginHistory;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.utilities.Utils;
import com.network.mysalon.activities.salon.VendorDashboardActivity;
import com.network.mysalon.activities.user.UserDashboardActivity;
import com.network.mysalon.databinding.ActivityLoginBinding;
import com.network.mysalon.models.UserModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding mBinding;
    private Dialog d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        setSpinner();
        clickListeners();

    }

    private void setSpinner() {
        List<String> items = new ArrayList<>();
        items.add("User");
        items.add("Vendor");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(LoginActivity.this, R.layout.list_item, items);
        mBinding.roleSpinner.setAdapter(adapter);
    }

    private void clickListeners() {

        mBinding.signInButton.setOnClickListener(v -> {
            if (TextUtils.isEmpty(mBinding.emailField.getText()) ||
                    TextUtils.isEmpty(mBinding.passwordField.getText()) ||
                    TextUtils.isEmpty(mBinding.roleSpinner.getText())) {
                Toast.makeText(this, "All fields are required!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!Utils.isValidEmail(mBinding.emailField.getText().toString().trim())) {
                Toast.makeText(this, "Invalid email address!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(mBinding.passwordField.getText()) || mBinding.passwordField.length() < 6 || mBinding.passwordField.length() > 15) {
                Toast.makeText(this, "Password should be 6-15 characters long!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (mBinding.roleSpinner.getText().toString().trim().equals("")) {
                Toast.makeText(this, "Please select role", Toast.LENGTH_SHORT).show();
                return;
            }

            d = new Dialog(this);
            d.setContentView(R.layout.dialog_loader);
            d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            d.setCancelable(false);
            d.setCanceledOnTouchOutside(false);

            d.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    loginWithFirebase();
                }
            });

            d.show();
        });

        mBinding.signUpText.setOnClickListener(v -> {
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        });

    }

    private void loginWithFirebase() {

        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signInWithEmailAndPassword(mBinding.emailField.getText().toString(), mBinding.passwordField.getText().toString())
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        d.dismiss();
                        Toast.makeText(LoginActivity.this, "Login failed.", Toast.LENGTH_SHORT).show();
                    } else {

                        FirebaseDatabase.getInstance().getReference("users/" + mBinding.roleSpinner.getText().toString().toLowerCase())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        for (DataSnapshot userSnapshot : snapshot.getChildren()) {
                                            final UserModel[] userModel = {userSnapshot.getValue(UserModel.class)};
                                            if (userModel[0].getEmailAddress().equals(mBinding.emailField.getText().toString())) {

                                                FirebaseInstanceId.getInstance().getInstanceId()
                                                        .addOnCompleteListener(task -> {
                                                            if (!task.isSuccessful()) {
                                                                Log.w("TAG", "Fetching FCM registration token failed", task.getException());
                                                                d.dismiss();
                                                                return;
                                                            }

                                                            String token = task.getResult().getToken();
                                                            Log.d("TAG", "generateToken: " + token);

                                                            FirebaseDatabase.getInstance().getReference().child("users/" + userModel[0].getRole() + "/" + userModel[0].getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                                    userModel[0] = snapshot.getValue(UserModel.class);
                                                                    userModel[0].setFcmToken(token);
                                                                    String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date());
                                                                    userModel[0].getLoginHistory().add(new LoginHistory(date, ""));
                                                                    new UserPreferences(LoginActivity.this).setUser(userModel[0]);
                                                                    FirebaseDatabase.getInstance().getReference().child("users/" + userModel[0].getRole() + "/" + userModel[0].getId()).setValue(userModel[0]);

                                                                    if (userModel[0].getRole().equals("vendor"))
                                                                        startActivity(new Intent(LoginActivity.this, VendorDashboardActivity.class)
                                                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                                    else
                                                                        startActivity(new Intent(LoginActivity.this, UserDashboardActivity.class)
                                                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                                    d.dismiss();
                                                                    finish();
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError error) {
                                                                    d.dismiss();
                                                                }
                                                            });
                                                        });
                                                break;
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {
                                        d.dismiss();
                                        Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                });

    }
}