package com.network.mysalon.activities.salon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.network.mysalon.fragments.MenuFragment;
import com.network.mysalon.R;
import com.network.mysalon.fragments.NotificationFragment;
import com.network.mysalon.fragments.salon.SalonServicesFragment;
import com.network.mysalon.utilities.UserPreferences;
import com.network.mysalon.fragments.BookingFragment;
import com.network.mysalon.databinding.ActivityVendorDashboardBinding;
import com.network.mysalon.models.Notifications;
import com.network.mysalon.models.UserModel;

public class VendorDashboardActivity extends AppCompatActivity {

    private static final int NUM_PAGES = 4;
    public PagerAdapter pagerAdapter;
    public ActivityVendorDashboardBinding mBinding;
    UserModel userModel;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {

        Bundle bundle = getIntent().getExtras();
//        android.os.Debug.waitForDebugger();
        if (bundle != null) {
            if (bundle.containsKey("notification")) {
                Notifications notification = (Notifications) bundle.getSerializable("notification");
                FirebaseDatabase.getInstance().getReference().child("users/" + userModel.getRole() + "/" + userModel.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        userModel = snapshot.getValue(UserModel.class);
                        userModel.getNotifications().add(notification);
                        new UserPreferences(VendorDashboardActivity.this).setUser(userModel);
                        FirebaseDatabase.getInstance().getReference().child("users/" + userModel.getRole() + "/" + userModel.getId()).setValue(userModel);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                mBinding.viewPager.setCurrentItem(2);
            }
        }
//        else
//            mBinding.viewPager.setCurrentItem(0);

        refreshBadge();

        super.onResume();
    }

    public void refreshBadge() {

        int count = 0;
        userModel = new UserPreferences(this).getUser();
        for (Notifications item : userModel.getNotifications()) {
            if (!item.isNotificationRead())
                count += 1;
        }
        if (count > 0) {
            BadgeDrawable badge = mBinding.bottomBar.getOrCreateBadge(R.id.notification_fragment);
            badge.setVisible(true);
            badge.setNumber(count);
            badge.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    public BadgeDrawable getBadge() {
        return mBinding.bottomBar.getOrCreateBadge(R.id.notification_fragment);
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (currentFragment == null) {
            if (mBinding.viewPager.getCurrentItem() == 0) {
                super.onBackPressed();
            } else {
                mBinding.viewPager.setCurrentItem(0);
            }
        } else {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_vendor_dashboard);
        userModel = new UserPreferences(this).getUser();
        getSupportFragmentManager().addOnBackStackChangedListener(getListener());
        pagerAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @NonNull
            @Override
            public Fragment getItem(int position) {
                Fragment mFragment;
                if (position == 0)
                    mFragment = new BookingFragment();
                else if (position == 1)
                    mFragment = new SalonServicesFragment();
                else if (position == 2)
                    mFragment = new NotificationFragment();
                else
                    mFragment = new MenuFragment();
                return mFragment;
            }

            @Override
            public int getCount() {
                return NUM_PAGES;
            }
        };
//        mBinding.viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mBinding.viewPager.setAdapter(pagerAdapter);

        mBinding.bottomBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.bookings_fragment) {
                    mBinding.viewPager.setCurrentItem(0);
                    return true;
                } else if (item.getItemId() == R.id.services_fragment) {
                    mBinding.viewPager.setCurrentItem(1);
                    return true;
                } else if (item.getItemId() == R.id.notification_fragment) {
                    mBinding.viewPager.setCurrentItem(2);
                    return true;
                } else if (item.getItemId() == R.id.menu_fragment) {
                    mBinding.viewPager.setCurrentItem(3);
                    return true;
                }
                return false;
            }
        });

        mBinding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mBinding.bottomBar.setSelectedItemId(position == 0 ? R.id.bookings_fragment :
                        position == 1 ? R.id.services_fragment :
                                position == 2 ? R.id.notification_fragment : R.id.menu_fragment);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                FragmentManager manager = getSupportFragmentManager();

                if (manager != null) {
                    Fragment currFrag = manager.findFragmentById(R.id.viewPager);

                    if (currFrag != null)
                        currFrag.onResume();
                }
            }
        };

        return result;
    }
}